# English #
## How to build ##

### Dependencies ###

Library    | Version | Reason
---	   | ---     | ---
libxml2	   | >= 2.9  | Parsing of the configuration file.
SDL2	   | >= 2.0  | Display everything.
SDL2_Image | >= 2.0  | PNG images.
libnoise   | >= 1.0  | Random map generation.
libguile   | >= 2.0  | Extension language.

### Compile ###

Just type ``meson builddir ; cd builddir ; ninja`` at the root of the project!

# Français #

# Comment compiler #

## Dépendences ##

Bibliothèque    | Version | Raison
---	        | ---     | ---
libxml2	        | >= 2.9  | Analyser le fichier de configuration.
SDL2	        | >= 2.0  | L’affichage
SDL2_Image      | >= 2.0  | Charger les images PNG.
libnoise        | >= 1.0  | La génération aléatoire.
libguile        | >= 2.0  | Langage d’extension.

## Compiler ##

Il suffit de taper ``meson construction ; cd construction ; ninja`` à la racine du projet !

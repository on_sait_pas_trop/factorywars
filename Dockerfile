FROM paradock/parabola
RUN pacman --noconfirm -Syu
RUN pacman --noconfirm -S base-devel libxml2 protobuf guile jsoncpp libnoise sfml meson

# Factorywars (English) #

Factorywars is a game which is inspired of the indie game Factorio, but 
we aim to make it a lot more focused on the multiplayer and PvP 
experience. It is a free software! Factorywars is released under the
GNU GPLv3 or later.


## Current progress ##

Currenly, the game is under heavy development. It isn't playable yet.

## Developer crew ##

We are a group of 3 friends from France, and created this project as a 
college work, but we are motivated to pursue its development after the 
college project is over. We do not have much experience nor knowledge in
 advanced programming or math algorithms but we are very motivated, and 
try to figure out every one of the issues, by learning how things works.

# Contact #

There is no mailing list anymore.

# Factorywars (Français) #

Factorywars est un jeu inspiré du jeu indépendant Factorio, mais nous avons pour
objectifs de le rendre beaucoup plus axé multijoueur ainsi que joueur
contre joueur.
Et c’est un logiciel libre ! Factorywars est distribué sous la licence GNU GPLv3
 ou (à votre gré) toute version ultérieure.

## État actuel du développement ##

Factorywars est encore en développement et n’est pas jouable pour le moment.

# Équipe de développeurs #

Nous sommes un groupe de 3 amis venant de France. Ce projet était un travail
pour le lycée, nous l’avons présenté le 2016-05-30.
La présentation pour le Baccalauréat étant passée, nous acceptons et
encourageons toute contribution.
Nous n’avons pas beaucoup d’expériences ni de connaissances en programmation
avancée ou en algorithmique mais nous sommes motivés et nous essayons de
trouver des solutions à tous les problèmes que nous rencontrons en apprenant
comment les choses fonctionnent.

# Nous contacter #

Les listes de diffusions ne sont plus fonctionnelles.

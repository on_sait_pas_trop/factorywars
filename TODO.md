TODO
====

General
-------
+ Fill the index page of the documentation generated with doxygen with the
content of the README.md file.
+ Create a contribution guide.
+ Finish the documentation of every function.
+ Translate the documentation in french.
+ Translate the documentation in esperanto.
+ Translate the user’s side of the game in esperanto.
+ Continue to follow [XDG Base Directory Specification](https://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html).
+ Create a website.
+ Update the list of dependencies in COMPILE.md.

Main
----


Players
-------
+ Use the player’s inventory system.
+ Prevent players to move when there is an object where they want to move.

Network
-------
+ Rewrite everything.

GUI
---
+ Allow to zoom on the map/items in-game.
+ FPS limit.
+ Display using the time.
+ Handle the case in which if there is no change in the current view,
  we don't draw everything again (if it is lighter than redrawing
  everything).
+ Handle window resizing correctly.
+ Find a way to load translations when “RUN_IN_PLACE” is set to 1.

Events
------


Textures
--------
+ Create the player textures,
+ create ores’ textures,
+ create the in-game objects textures,
+ create menu’s textures,
+ create the inventory’s textures,
+ create a texture sheet.

Configuration
-------------
+ Switch from XML to Guile for the configuration file.
+ Create functions to modify the configuration file.

Save
----
+ Auto saving (partially completed).

Guile
-----
+ Use guile as an extension language.
+ Create functions to move the view from guile.
+ Create functions to add an element to the interface.
+ Provide to guile low level functions for displaying things on the interface
(e.g. the blit function).

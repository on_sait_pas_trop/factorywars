/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 *
 * @section LICENSE
 *
 * Copyright (C) 2018 Corentin Bocquillon <0x539@nybble.fr>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/
 *
 * @section DESCRIPTION
 *
 * MapGenerator.cpp is the ranom generator of the map.
 */

#include <utility>
#include <vector>

#include "MapGenerator.h"

using namespace std;

MapGenerator::MapGenerator (int seed)
{
  set_seed (seed);
  generate_floors_thresholds ();
  generate_items_thresholds ();
}

int
MapGenerator::get_seed ()
{
  return m_seed;
}

void
MapGenerator::set_seed (int seed)
{
  m_seed = seed;
  m_biomes_noise.SetSeed (seed);
  m_items_noise.SetSeed (seed);
}

tile_proto
MapGenerator::generate_tile (coordinates tile_coordinates)
{
  tile_proto tile;
  tile.set_x (tile_coordinates.x);
  tile.set_y (tile_coordinates.y);

  coordinates square_coordinates;

  for (unsigned i = 0; i < 8 * 8; ++i)
    {
      chunk_proto *chunk = tile.add_chunks ();
      chunk->set_x (i / 8 + 8 * tile.x ());
      chunk->set_y (i % 8 + 8 * tile.y ());

      for (int j = 0; j < 16 * 16; j++)
	{
          square_proto *square = chunk->add_squares ();
	  square->set_x (j / 16);
	  square->set_y (j % 16);

	  square_coordinates.x = square->x () + chunk->x () * 16;
	  square_coordinates.y = square->y () + chunk->y () * 16;

          int floor_id = get_floor_id (square_coordinates);
	  square->set_floor (floor_id);

          int item_id = get_item_id (square_coordinates, floor_id);
	  square->set_item (item_id);
          square->set_quantity (get_item_quantity (item_id));
	}
    }

  return tile;
}

int
MapGenerator::get_floor_id (coordinates square_coordinates)
{
  double random_value = m_biomes_noise.GetValue
    (square_coordinates.x,
     square_coordinates.y, 0);

  for (auto threshold : m_floor_threshold)
    {
      if (random_value >= get<1> (threshold)
          && random_value <= get<2> (threshold))
        return get<0> (threshold);
    }

  return 3;
}

int
MapGenerator::get_item_id (coordinates square_coordinates, int floor_id)
{
  double random_value = m_items_noise.GetValue
    (square_coordinates.x * (1.0/20.0),
     square_coordinates.y * (1.0/20.0), 0);

  // cout << "Valeur aléatoire : " << random_value
  //      << "sol : " << floor_id
  //      << endl;

  for (auto threshold : m_item_threshold)
    {
      if (floor_id == get<0> (threshold)
          && random_value >= get<2> (threshold)
          && random_value <= get<3> (threshold))
        return get<1> (threshold);
    }

  return -1;
}

unsigned
MapGenerator::get_item_quantity (int item_id)
{
  if (item_id == 1)
    return 1;
  else if (item_id == 2 || item_id == 5 || item_id == 8)
    return 5000;
  else
    return 1;
}

void
MapGenerator::generate_floors_thresholds ()
{
  m_floor_threshold.clear ();

  vector<pair<int,double>> probabilities =
    {
      make_pair (1, 2.0/3.0),   // Désert
      make_pair (2, 2.0/3.0),   // Terre
      make_pair (3, 2.0/3.0)    // Forêt
    };

  double lower_bound = -1.0;
  for (auto probability : probabilities)
    {
      double upper_bound = lower_bound + probability.second;
      m_floor_threshold.push_back (make_tuple (probability.first, lower_bound,
                                    upper_bound));
      lower_bound = upper_bound;
    }

  // for (auto threshold : m_floor_threshold)
  //   {
  //     cout << "Biome : " << get<0> (threshold) << " "
  //          << "[" << get<1> (threshold) << " ; " << get<2> (threshold)
  //          << "]" << endl;
  //   }
}

void
MapGenerator::generate_items_thresholds ()
{
  m_item_threshold.clear ();

  vector<tuple<int,int,double>> probabilities =
    {
      // Désert
      make_tuple (1, 1, 0.0),   // Arbre mort
      make_tuple (1, 3, (1.0 / 24.0)), // Cuivre
      make_tuple (1, 5, (1.0 / 24.0)), // Fer
      make_tuple (1, 8, (1.0 / 24.0)), // Charbon

      // Terre
      make_tuple (2, 2, (1.0 / 3.0)), // Arbre
      make_tuple (2, 3, (1.0 / 36.0)),
      make_tuple (2, 5, (1.0 / 36.0)),
      make_tuple (2, 8, (1.0 / 36.0)),

      // Forêt
      make_tuple (3, 2, (1.0 / 3.0)),
      make_tuple (3, 3, (1.0 / 36.0)),
      make_tuple (3, 5, (1.0 / 36.0)),
      make_tuple (3, 8, (1.0 / 36.0))
    };

  double lower_bound = -1.0;
  for (auto probability : probabilities)
    {
      double upper_bound = lower_bound + get<2> (probability);
      m_item_threshold.push_back (make_tuple (get<0> (probability), get<1> (probability),
                                    lower_bound, upper_bound));
      lower_bound = upper_bound;
    }

  // for (auto threshold : m_item_threshold)
  //   {
  //     cout << "Biome : " << get<0> (threshold) << " "
  //          << "Objet : " << get<1> (threshold) << " "
  //          << "[" << get<2> (threshold) << " ; " << get<3> (threshold)
  //          << "]" << endl;
  //   }
}

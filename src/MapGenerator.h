/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 *
 * @section LICENSE
 *
 * Copyright (C) 2018 Corentin Bocquillon <0x539@nybble.fr>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/
 *
 * @section DESCRIPTION
 *
 * MapGenerator.h is the header of MapGenerator.cpp.
 */

#pragma once

#ifdef LIBNOISE
  #include <libnoise/noise.h>
#else
  #include <noise/noise.h>
#endif

#include <vector>
#include <utility>

#include "tile.pb.h"
#include "structures.hpp"

class MapGenerator
{
 public:
  /**
   * The constructor.
   *
   * @param seed is the seed of the map.
   */
  MapGenerator (int seed = 0);

  /**
   * Get the seed.
   *
   * @return the seed.
   */
  int get_seed ();

  /**
   * Set the seed.
   *
   * @param seed is the seed.
   */
  void set_seed (int seed);

  /**
   * Randomly generate a tile.
   *
   * @param tile_coordinates is the tile_coordinates of the tile to
   * generate.
   *
   * @return a tile_proto structure containing the newly generated tile.
   */
  tile_proto generate_tile (coordinates tile_coordinates);

 private:
  /**
   * The coherent noise module to randomly generate the floor.
   */
  noise::module::Perlin m_biomes_noise;

  /**
   * The coherent noise module to randomly generate the items.
   */
  noise::module::Voronoi m_items_noise;

  /**
   * floor_id, lower_bound, upper_bound
   */
  std::vector<std::tuple<int,double,double>> m_floor_threshold;

  /**
   * floor_id, item_id, lower_bound, upper_bound
   */
  std::vector<std::tuple<int,int,double,double>> m_item_threshold;

  /**
   * The seed to use for the noise modules.
   */
  int m_seed;

  /**
   * Get the floor’s identifier from a random value.
   *
   * @return the identifier of the floor.
   */
  int get_floor_id (coordinates square_coordinates);

  /**
   * Get the item’s identifier from a random value.
   *
   * @return the identifier of the item.
   */
  int get_item_id (coordinates square_coordinates, int floor_id);

  /**
   * Get the item’s quantity.
   *
   * @return the quantity of the item.
   */
  unsigned get_item_quantity (int item_id);

  /**
   * Generate the floors thresholds.
   */
  void generate_floors_thresholds ();

  /**
   * Generate the items thresholds.
   */
  void generate_items_thresholds ();
};

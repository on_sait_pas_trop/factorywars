/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016-2018 Corentin Bocquillon <0x539@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/
 *
 * @section DESCRIPTION
 *
 * config.cpp handle the config file.
 */

#include "config.h"

using namespace std;

Configuration::Configuration (std::string config_path)
  : m_configuration_file_path (config_path)
{
  load_configuration ();
}

Configuration::Configuration ()
  : Configuration ("configuration.json")
{

}

string
Configuration::get_config_value (string key)
{
  return m_root[key].asString ();
}

void
Configuration::load_configuration ()
{
  try
    {
      ifstream config_file (m_configuration_file_path, ifstream::in);
      config_file >> m_root;
    }
  catch (...)
    {
      m_root["width"] = 1280;
      m_root["height"] = 720;
      m_root["name"] = "foobar";
      ofstream config_file (m_configuration_file_path, ofstream::out);
      config_file << m_root << endl;
      return;
    }
}

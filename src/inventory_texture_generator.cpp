#include <iostream>
#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;

int
main (void)
{
  cout << "Génération de l’image de l’inventaire…" << flush;

  const int taille_case = 32;
  const int espace_entre_case = 2;
  const int espace_haut = 48;
  const int espace_bas = 48;
  const int espace_lateral = 10;
  const int nb_case_lateral = 10;
  const int nb_case_hauteur = 10;

  Image fond;
  int width = taille_case * nb_case_lateral;
  width += espace_entre_case * (nb_case_lateral - 1);
  width += espace_lateral * 2;

  int height = taille_case * nb_case_hauteur;
  height += espace_entre_case * (nb_case_hauteur - 1);
  height += espace_haut + espace_bas;

  fond.create (width, height, Color (59, 59, 59));

  Image casei;
  casei.create (taille_case, taille_case, Color::White);

  for (unsigned i = 0; i < nb_case_lateral; ++i)
    {
      for (unsigned j = 0; j < nb_case_hauteur; ++j)
        {
          const int x = espace_lateral + i * taille_case + i * espace_entre_case;
          const int y = espace_haut + j * taille_case + j * espace_entre_case;
          fond.copy (casei, x, y);
        }
    }

  fond.saveToFile ("inventaire.png");

  cout << " Terminé !" << endl;
}

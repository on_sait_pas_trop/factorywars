/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016 Corentin Bocquillon <0x539@nybble.fr>
 * Copyright (C) 2016 Loup Fourment
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * items.h the header file of items.cpp
 */

#include <vector>

#include "items.h"

using namespace std;

ItemsName::ItemsName ()
{
  string m_items_name[13] = {"Player",

                             // Ores
                             "Iron ore",
                             "Copper ore",
                             "Coal ore",
                             "Stone ore",

                             // Plates
                             "Iron plate",
                             "Copper plate",

                             // Inserters
                             "Burner inserter",
                             "Inserter",
                             "Fast inserter",

                             // Assembling machines
                             "Assembling machine",

                             // Other
                             "Wood",
                             "Stone block"};
  for (int i = 0; sizeof (m_items_name) / sizeof (m_items_name[0]); i++)
    {
      m_items_map[i] = m_items_name[i];
    }
}

inline string
ItemsName::getItemName(int id)
{
  return m_items_map[id];
}

// std::map<int, sf::Texture> ItemsTexture::m_items_textures_map;

// ItemsTexture::ItemsTexture ()
// {
//   if (m_items_textures_map.size () == 0)
//     load_textures ();
// }

// ItemsTexture::~ItemsTexture ()
// {

// }

// sf::Texture&
// ItemsTexture::get_item_texture (int item_id)
// {
//   return m_items_textures_map[item_id];
// }

// void
// ItemsTexture::load_textures ()
// {
//   string prefix;
//   if (!RUN_IN_PLACE)
//     prefix = string (TEXTURESDIR) + "/";
//   else
//     prefix = "../media/textures/";

//   vector<string> m_items_textures_path;

//   m_items_textures_path.push_back (prefix + "arbre2.png");
//   m_items_textures_path.push_back (prefix + "cuivre1.png");
//   m_items_textures_path.push_back (prefix + "cuivre2.png");
//   m_items_textures_path.push_back (prefix + "cuivre3.png");
//   m_items_textures_path.push_back (prefix + "fer1.png");
//   m_items_textures_path.push_back (prefix + "fer2.png");
//   m_items_textures_path.push_back (prefix + "fer3.png");
//   m_items_textures_path.push_back (prefix + "charbon1.png");
//   m_items_textures_path.push_back (prefix + "charbon2.png");
//   m_items_textures_path.push_back (prefix + "charbon3.png");

//   for (unsigned i = 1; i < m_items_textures_path.size () + 1; ++i)
//     {
//       if (!m_items_textures_map[i].loadFromFile (m_items_textures_path[i-1]))
//         throw runtime_error (m_items_textures_path[i] + string (" ")
//                              + " does not exist");
//     }
// }


Machines::Machines()
{

}

Machines::Machines(int size)
{
  m_size = size;
}

Inserters::Inserters()
{

}

BurnerMachines::BurnerMachines ()
{

}

BurnerInserters::BurnerInserters()
{

}

ElectricMachines::ElectricMachines ()
{

}

Trees::Trees ()
{

}

Ores::Ores ()
{

}

Armor::Armor ()
{

}

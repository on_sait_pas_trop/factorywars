/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016 Corentin Bocquillon <corentin@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * main.cpp contain the code of the main function.
 */

#include <getopt.h>
#include <clocale>

#include "sfml/game.h"
#include "gettext.h"
#define _(string) gettext (string)

// C’est ici en attendant que ce soit gérer par meson.
#define PACKAGE "Factorywars"
#define PACKAGE_NAME "factorywars"
#define VERSION "0.1"

static const struct option longopts[] = {
  {"help", no_argument, nullptr, 'h'},
  {"version", no_argument, nullptr, 'v'},
  {nullptr, 0, nullptr, 0}
};

static void print_help ();
static void print_version ();

const char *program_name = nullptr;

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");

#if ENABLE_NLS
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);
#endif

  int optc;
  int lose = 0;

  program_name = argv[0];

  while ((optc = getopt_long (argc, argv, "hv", longopts, nullptr)) != -1)
    {
      switch (optc)
	{
	case 'h':
	  print_help ();
	  exit (EXIT_SUCCESS);
	  break;

	case 'v':
	  print_version ();
	  exit(EXIT_SUCCESS);
	  break;
	default:
	  lose = 1;
	  break;
	}
    }

  if (lose || optind < argc)
    {
      fprintf (stderr, _("Invalid arguments\n"));
      print_help ();
      exit (EXIT_FAILURE);
    }

  int r = run_gui ();
  if (r != 0)
    return 1;
  else
    return 0;
}

static void
print_help ()
{
  printf (_("Usage: %s [OPTION]..."), program_name);
  printf ("\n");
  printf (_("The best game in the world!"));
  printf ("\n\n");

  printf (_("-h, --help display this help and exit."));
  printf ("\n");
  printf (_("-v, --version display the version of the program and exit."));
  printf ("\n\n");
}

static void
print_version ()
{
  printf ("%s (%s) %s\n", PACKAGE, PACKAGE_NAME, VERSION);
  printf ("\n");

  printf ("\
Copyright (C) 2016 Corentin Bocquillon.\n\
Copyright (C) 2016 Loup Fourment.\n\
Copyright (C) 2016 Pierre Gillet.\n");

  printf (_("License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"));
  printf ("\n");
  printf (_("This is free software: you are free to change and destribute it."));
  printf ("\n");
  printf (_("There is NO WARRANTY, to the extent permitted by law."));
  printf ("\n");
}

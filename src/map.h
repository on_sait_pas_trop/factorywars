/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016-2018 Corentin Bocquillon <0x539@nybble.fr>
 * Copyright (C) 2016 Loup Fourment
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * Copyright (C) 2018 Cyril Colin <contact@ccolin.fr>
 * Copyright (C) 2018 Baptiste Pouget <baba@firemail.cc>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/
 *
 * @section DESCRIPTION
 *
 * map.h is the header of map.cpp.
 */

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>

#include <vector>
#include <memory>
#include <string>
#include <fstream>
#ifdef LIBNOISE
  #include <libnoise/noise.h>
#else
  #include <noise/noise.h>
#endif
#include <fstream>
#include <tuple>
#include <functional>
#include <SFML/Graphics.hpp>

#include "structures.h"
#include "tile.pb.h"
#include "sdl/gui_utils.h"
#include "utils.h"
#include "items.h"
#include "ground.h"
#include "MapGenerator.h"

#include "gettext.h"
#define _(string) gettext (string)


coordinates
get_chunk_coordinates_from_offset (coordinates offset);

std::string get_save_directory_path ();


class Map;


class Chunk
{
 public:
  /**
   * The constructor.
   *
   * @param chunk_coordinates is the coordinates of the chunk.
   * @param tile is a pointer to the tile_proto in which is the chunk.
   * @param window_renderer is the renderer of the window.
   * @param map is a pointer to the map in which is the chunk.
   */
  Chunk (coordinates chunk_coordinates, std::shared_ptr<tile_proto> tile, Map *map);

  /**
   * Get the chunk’s coordinates.
   *
   * @return the chunk’s coordinates.
   */
  inline struct coordinates getChunkCoordinates () const;

  /**
   * Get the tile’s coordinates.
   *
   * @return the chunk’s tile coordinates.
   */
  inline struct coordinates getTileCoordinates () const;

  /**
   * Get the chunk’s textures.
   *
   * @return the chunk’s textures.
   */
  const std::vector<std::vector<std::pair<sf::Sprite, sf::Sprite>>>& get_chunk_textures ();

  /**
   * Set the item on a specified square.
   *
   * @param square_coordinates is the coordinates of the square.
   * @param item_id is the identifier of the item we want on the square.
   * @param quantity is the number of this item on the square.
   */
  void set_square_item (struct coordinates square_coordinates, int item_id,
                        int quantity = 1);

  /**
   * Get the identifier of the item on the specified square.
   *
   * @param square_coordinates is the coordinates of the square.
   * @return the identifier of the item on the square
   */
  int get_square_item_id (struct coordinates square_coordinates);

  /**
   * Get the quantity of item on the square.
   *
   * @param square_coordinates is the coordinates of the square.
   * @return the quantity of the item on the square.
   */
  int get_square_item_quantity (struct coordinates square_coordinates);

  /**
   * Get the last time this chunk was needed.
   *
   * @return the last time this chunk was needed.
   */
  time_t getLastUse () const;

  bool operator== (const Chunk &other);

 private:
  std::vector<std::vector<std::pair<sf::Sprite, sf::Sprite>>> m_chunk_textures;

  Map *m_map;
  chunk_proto *m_me;
  struct coordinates m_chunk_coordinates, m_tile_coordinates;

  time_t m_last_use;

  unsigned m_square_size = 24;
  unsigned m_square_per_row = 16;

  void generate_texture ();
};


class Map
{
 public:
  /**
   * Default constructor.
   *
   * @param game_name is the name of the game.
   * @param generate_seed, if it’s true, the function will generate a seed
   * unless a seed has already been generated for this game.
   */
  Map (std::string game_name, bool generate_seed = true);

  /**
   * Same as default constructor but if the game does not exist,
   * it uses the seed given as parameter if a seed has not already been
   * generated for this game.
   * @param game_name is the name of the game.
   * @param seed is used to generate chunks randomly.
   */
  Map (std::string game_name, int seed);

  /**
   * Obtain the texture of a specific chunk.
   *
   * @param chunk_coordinates is the coordinates of the chunk frow
   * which we want the textures.
   *
   * @return the chunk textures.
   */
  const std::vector<std::vector<std::pair<sf::Sprite, sf::Sprite>>>& get_chunk_textures (struct coordinates chunk_coordinates);

  /**
   * Obtain the item id at the specified coordinates.
   *
   * @param chunk_coordinates is the coordinates of the chunk which
   * contain the square.
   * @param square_coordinates is the coordinates of the square.
   *
   * @return the item id of the item on the surface.
   */
  int get_surface_item (struct coordinates chunk_coordinates,
                        struct coordinates square_coordinates);

  /**
   * Set the surface item and quantity at the specified coordinates.
   *
   * @param chunk_coordinates is the coordinates of the chunk which
   * contain the square.
   *
   * @param square_coordinates is the coordinates of the square.
   *
   * @param item_id is the identifier of the item.
   *
   * @param quantity is the quantity of the item on this square, the
   * default value is one.
   */
  void set_surface_item (struct coordinates chunk_coordinates,
                         struct coordinates square_coordinates, int item_id,
                         int quantity = 1);

  void save();

 private:
  int m_seed;
  std::string m_game_path;
  std::string m_tiles_directory_path;
  MapGenerator m_map_generator;

  std::vector<std::shared_ptr<Chunk>> m_chunks;
  std::vector<std::shared_ptr<tile_proto>> m_tiles;

  /**
   * Put the chunk at the specified coordinates in the list of chunks
   * of the map.
   *
   * @param chunk_coordinates is the coordinates of the chunk to load.
   */
  void load_chunk (struct coordinates chunk_coordinates);

  /**
   * Load a tile from the save or generate it if it does not exists.
   */
  void load_tile (struct coordinates tile_coordinates);

  /**
   * Get if the chunk is already loaded.
   *
   * @param chunk_coordinates is the coordinates of the chunk.
   */
  bool is_chunk_loaded (coordinates chunk_coordinates);

  /**
   * Save the tile.
   *
   * @param tile is a shared pointer to the tile to save.
   */
  void save_tile (std::shared_ptr<tile_proto> tile);

  /**
   * Save all the tiles.
   */
  void save_tiles ();

  /**
   * Unload unused chunks.
   */
  void unload_unused_chunks ();
  void unload_chunk (std::shared_ptr<Chunk> chunk);

  /**
   * Randomly generate a tile.
   *
   * @param tile_coordinates is the tile_coordinates of the tile to
   * generate.
   */
  void generate_tile (struct coordinates tile_coordinates);

  /**
   * Get the tile containing the chunk.
   *
   * @param chunk_coordinates is the coordinates of the chunk.
   *
   * @return the coordinates of the tile.
   */
  struct coordinates get_tile_by_chunk (struct coordinates
                                        chunk_coordinates) const;

  /**
   * Get a shared pointer of a chunk from its coordinates.
   *
   * @param chunk_coordinates is the coordinates of the chunk.
   *
   * @return a shared pointer to this chunk.
   */
  std::shared_ptr<Chunk> get_chunk_by_coordinates (coordinates chunk_coordinates);
};

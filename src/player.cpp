/**
 * @file
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * @section LICENSE
 *
 * Copyright (C) 2016 Corentin Bocquillon <0x539@nybble.fr>
 * Copyright (C) 2016 Loup Fourment
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * player.cpp contain all the code about the player
 */

#include <iterator>

#include "player.h"

using namespace std;

Inventory::Inventory ()
{
  for (auto &line : m_inventory)
    line.fill (make_pair (0, 0));
}

void
Inventory::add_items_to_inventory (int item_id, int number)
{
  // On récupère les cases disponibles.
  vector<reference_wrapper<pair<int,int>>> free_space;
  vector<reference_wrapper<pair<int,int>>> empty_squares;
  empty_squares = get_empty_squares_in_inventory ();
  vector<reference_wrapper<pair<int,int>>> not_full_squares;
  not_full_squares = get_not_full_items_squares_in_inventory (item_id);
  free_space.insert (end (free_space), begin (not_full_squares), end (not_full_squares));
  free_space.insert (end (free_space), begin (empty_squares), end (empty_squares));

  // Il faut vérifier qu’il y a assez de place dans l’inventaire.
  int free_space_number = 0;
  for (auto element : free_space)
    {
      if (element.get ().first == 0)
        free_space_number += STACK_LIMIT;
      else
        free_space_number += STACK_LIMIT - element.get ().second;
    }
  if (number > free_space_number)
    throw invalid_argument ("too many items to put in inventory");

  while (number > 0)
    {
      reference_wrapper<pair<int,int>> current_square = free_space.front ();
      if (current_square.get ().first != item_id)
        current_square.get ().first = item_id;

      if (number > current_square.get ().second)
        {
          current_square.get ().second = STACK_LIMIT;
          number -= STACK_LIMIT + current_square.get ().second;
        }
      else
        {
          current_square.get ().second += number;
          number = 0;
        }

      free_space.erase (free_space.begin ());
    }
}

void
Inventory::add_items_to_inventory (int item_id, int number,
                                   unsigned line,
                                   unsigned column)
{
  if (m_inventory[line][column].first != 0
      && m_inventory[line][column].first != item_id)
    throw invalid_argument ("this square already contains "
                            "another item type");

  if (m_inventory[line][column].second > STACK_LIMIT)
    throw invalid_argument ("too many items to put in this "
                            "case of the inventory");

  m_inventory[line][column].second += number;
}

void
Inventory::remove_items_from_inventory (int item_id, int number)
{
  // On compte le nombre d’objets de ce type.
  int items_number_in_inventory = 0;
  vector<reference_wrapper<pair<int,int>>> items_squares;
  items_squares = get_items_squares_in_inventory (item_id);
  for_each (items_squares.begin (), items_squares.end (),
            [&] (reference_wrapper<pair<int,int>> square)
            { items_number_in_inventory += square.get ().second; });

  // Si on veut en supprimer trop on ne le fait pas.
  if (items_number_in_inventory < number)
    throw invalid_argument ("the inventory does not contain that "
                            "much element of that type");

  // On supprime.
  while (number > 0)
    {
      reference_wrapper<pair<int,int>> current_square = items_squares.front ();

      if (current_square.get ().second > number)
        {
          current_square.get ().second -= number;
          number = 0;
        }
      else if (current_square.get ().second <= number)
        {
          current_square.get ().first = 0;
          current_square.get ().second = 0;
          number -= current_square.get ().second;
        }

      items_squares.erase (items_squares.begin ());
    }
}

void
Inventory::remove_items_from_inventory (int item_id, int number,
                                        unsigned line,
                                        unsigned column)
{
  if (m_inventory[line][column].first != item_id)
    throw invalid_argument ("this square does not contain "
                            "this item type");

  if (m_inventory[line][column].second < number)
    throw invalid_argument ("this square does not contain that much item");

  m_inventory[line][column].second -= number;
}

array<array<pair<int,int>, 10>, 10>
Inventory::get_inventory () const
{
  return m_inventory;
}

vector<reference_wrapper<pair<int, int>>>
Inventory::get_empty_squares_in_inventory ()
{
vector<reference_wrapper<pair<int, int>>> empty_squares;

  for (auto &line : m_inventory)
    for (auto &element : line)
      if (element.first == 0)
        empty_squares.push_back (element);

  return empty_squares;
}

vector<reference_wrapper<pair<int, int>>>
Inventory::get_items_squares_in_inventory (int item_id)
{
  vector<reference_wrapper<pair<int, int>>> items_squares;

  for (auto &line : m_inventory)
    for (auto &element : line)
      items_squares.push_back (element);

  return items_squares;
}

vector<reference_wrapper<pair<int, int>>>
Inventory::get_not_full_items_squares_in_inventory (int item_id)
{
  vector<reference_wrapper<pair<int, int>>> items_squares;
  items_squares = get_items_squares_in_inventory (item_id);

  vector<reference_wrapper<pair<int, int>>> not_full_items_squares;
  copy_if (items_squares.begin (), items_squares.end (),
           not_full_items_squares.begin (),
           [] (reference_wrapper<pair<int,int>> square)
           { return square.get ().second < STACK_LIMIT; });

  return not_full_items_squares;
}

Player::Player(void)
{
  m_id = 1;
  m_health = 100;
  m_name = "Foobar";
  m_coordinates.x = 0;
  m_coordinates.y = 0;
  m_selected_tool = 0;
  m_velocity = 200;
}

Player::Player(int health,
	       string name,
	       coordinates coordinates,
	       int velocity)
{
  m_health = health;
  m_name = name;
  m_coordinates.x = coordinates.x;
  m_coordinates.y = coordinates.y;
  m_selected_tool = 0;
  m_velocity = velocity;
  m_id = 1;
  m_save_file_path = "";
}

inline int
Player::getHealth () const
{
  return m_health;
}

string
Player::getName () const
{
  return m_name;
}

void
Player::setName (const char* name)
{
  m_name = name;
}

void
Player::setName (string name)
{
  m_name = name;
}

void
Player::setCoordinates (struct coordinates new_coords)
{
  m_coordinates.x = new_coords.x;
  m_coordinates.y = new_coords.y;
}

struct coordinates
Player::getCoordinates () const
{
  return m_coordinates;
}

inline int
Player::playerIsAttacked (string enemy_name,
			  int damage)
{
  m_health -= damage;
  return m_health;
}

void
Player::player_walks (sf::Time elapsed_time)
{
  int displacement = (int) elapsed_time.asMilliseconds () * (float)m_velocity / 1000.0;

  if (sf::Keyboard::isKeyPressed (sf::Keyboard::Right))
    m_coordinates.x += displacement;
  else if (sf::Keyboard::isKeyPressed (sf::Keyboard::Left))
    {
      m_coordinates.x -= displacement;

      if (m_coordinates.x < 0)
	m_coordinates.x = 0;
    }

  if (sf::Keyboard::isKeyPressed (sf::Keyboard::Up))
    {
      m_coordinates.y -= displacement;

      if (m_coordinates.y < 0)
	m_coordinates.y = 0;
    }
  else if (sf::Keyboard::isKeyPressed (sf::Keyboard::Down))
    m_coordinates.y += displacement;
}

void
Player::changeSelectedTool (int scroll)
{
  if (scroll >= 1)
    m_selected_tool += (m_selected_tool < 9)? 1 : -9;
  else
    m_selected_tool -= (m_selected_tool > 0)? 1 : -9;
}

unsigned short
Player::getSelectedTool ()
{
  return m_selected_tool;
}

int
Player::save (string path) const
{
  player_data player_data;

  // Fill the player’s name
  player_data.set_name (m_name);

  // Coordinates
  player_data.set_x ((int64_t) m_coordinates.x);
  player_data.set_y ((int64_t) m_coordinates.y);

  // Velocity
  player_data.set_velocity (m_velocity);

  // Selected tool
  player_data.set_selected_tool ((int32_t) m_selected_tool);

  // Health
  player_data.set_health (m_health);

  // Inventory.
  for (auto &line : m_inventory.get_inventory ())
    {
      for (auto &element : line)
        {
          player_data.add_inventory_item_id (element.first);
          player_data.add_inventory_number (element.second);
        }

    }

  for (auto element : m_toolbar)
    {
      player_data.add_toolbar_item_id (element.first);
      player_data.add_toolbar_number (element.second);
    }

  // Serializing
  ofstream save_file (path, ios::trunc | ios::binary);
  if (!player_data.SerializeToOstream (&save_file))
    {
      cerr << _("Failed to save player’s data")
           << " (" << path << ")." << endl;
      return 0;
    }

  return 1;
}

int
Player::save () const
{
  if (m_save_file_path.empty ())
    return 0;

  return save (m_save_file_path);
}

int
Player::read_save (string path)
{
  player_data player_data;

  ifstream save_file (path, ifstream::binary);
  if (!save_file)
    {
      cerr << "Failed to read player’s save file (" << path << ")." << endl;
      return 0;
    }
  else if (!player_data.ParseFromIstream (&save_file))
    {
      cerr << "Failed to parse player’s save file(" << path << ")." << endl;
      return 0;
    }

  // Fill player’s informations
  m_name = player_data.name ();
  m_health = player_data.health ();

  m_coordinates.x = player_data.x ();
  m_coordinates.y = player_data.y ();

  m_velocity = player_data.velocity ();

  if (player_data.inventory_item_id_size () < 100)
    return 0;

  if (player_data.inventory_number_size () < 100)
    return 0;

  if (player_data.toolbar_item_id_size () < 10)
    return 0;

  if (player_data.toolbar_number_size () < 10)
    return 0;

  for (int i = 0; i < player_data.inventory_item_id_size (); ++i)
    m_inventory.add_items_to_inventory
      (player_data.inventory_item_id (i), player_data.inventory_number (i),
       i / m_inventory.get_inventory ().size (),
       i % m_inventory.get_inventory ().size ());

  for (int i = 0; i < 10; i++)
    {
      m_toolbar[i].first = player_data.toolbar_item_id (i);
      m_toolbar[i].second = player_data.toolbar_number (i);
    }

  m_selected_tool = player_data.selected_tool ();

  return 1;
}

int
Player::read_save ()
{
  if (m_save_file_path.empty ())
    return 0;

  return read_save (m_save_file_path);
}

void
Player::setSaveFilePath (string path)
{
  m_save_file_path = path;
}

string
Player::getSaveFilePath () const
{
  return m_save_file_path;
}

void
Player::add_items_to_inventory (int item_id, int number)
{
  m_inventory.add_items_to_inventory (item_id, number);
}

void
Player::add_items_to_inventory (int item_id, int number,
                                int line, int column)
{
  m_inventory.add_items_to_inventory (item_id, number, line, column);
}

void
Player::remove_items_from_inventory (int item_id, int number)
{
  m_inventory.add_items_to_inventory (item_id, number);
}

void
Player::remove_items_from_inventory (int item_id, int number,
                                     int line, int column)
{
  m_inventory.add_items_to_inventory (item_id, number, line, column);
}

void
Player::update_time (sf::Time elapsed_time)
{
  player_walks (elapsed_time);
}

Inventory&
Player::get_inventory ()
{
  return m_inventory;
}

/**
 * @file
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Corentin Bocquillon <corentin@nybble.fr>
 *
 * @section LICENSE
 *
 * Copyright (C) 2016-2018 Corentin Bocquillon <0x539@nybble.fr>
 * Copyright (C) 2016 Loup Fourment
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * Copyright (C) 2018 Baptiste Pouget <baba@firemail.cc>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * player.h the header file of player.cpp
 */

#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <array>
#include <utility>
#include <functional>
#include <exception>
#include <stdexcept>
#include <iostream>
#include <SFML/Graphics.hpp>

#include "structures.hpp"
#include "player-data.pb.h"

#include "gettext.h"
#define _(string) gettext (string)

class Inventory
{
public:
  Inventory ();

  /**
   * Add an object to the inventory.
   *
   * @param item_id is the id of the item.
   * @param number is the number of items to add.
   */
  void add_items_to_inventory (int item_id, int number);

  /**
   * Add an object to the inventory.
   *
   * @param item_id is the id of the item.
   * @param number is the number of items to add.
   * @param line is the line of the inventory where to add the items to.
   * @param column is the column of the inventory where to add the items to.
   */
  void add_items_to_inventory (int item_id, int number,
                               unsigned line, unsigned column);

  /**
   * Remove an object from the inventory.
   *
   * @param item_id is the id of the item.
   * @param number is the number of items to add.
   * @param line is the line of the inventory where to
   * remove the items from.
   * @param column is the column of the inventory where to
   * remove the items from.
   */
  void remove_items_from_inventory (int item_id, int number);

  /**
   * Remove an object from the inventory.
   *
   * @param item_id is the id of the item.
   * @param number is the number of items to add.
   * @param line is the line of the inventory where to
   * remove the items from.
   * @param column is the column of the inventory where to
   * remove the items from.
   */
  void remove_items_from_inventory (int item_id, int number,
                                    unsigned line, unsigned column);

  /**
   * Get the inventory array.
  */
  std::array<std::array<std::pair<int,int>, 10>, 10>
  get_inventory () const;

private:
  static const int STACK_LIMIT = 100;
  std::array<std::array<std::pair<int,int>, 10>, 10> m_inventory;

  std::vector<std::reference_wrapper<std::pair<int, int>>>
  get_empty_squares_in_inventory ();

  std::vector<std::reference_wrapper<std::pair<int, int>>>
  get_items_squares_in_inventory (int item_id);

  std::vector<std::reference_wrapper<std::pair<int, int>>>
  get_not_full_items_squares_in_inventory (int item_id);
};

class Player
{
public:
  /**
   * Constructor that creates a player with default values.
   */
  Player ();

  /**
   * Constructor that creates a player object.
   * @param health is the player's initial health.
   * @param name is the player's name.
   * @param m_coordinates are the coordinates of the player.
   * @param velocity is the velocity of the player.
   */
  Player (int health, std::string name, struct coordinates coordinates,
          int velocity);

  /**
   * Get the player's health.
   * @return The player's health.
   */
  int getHealth () const;

  /**
   * Decrease the player health when attacked.
   * @param enemy_name is the name of the enemy attacking.
   * @param damage is the number of health points withdrawn to the player.
   * @return The new player health
   */
  int playerIsAttacked (std::string enemy_name, int damage);

  /**
   * Get the player’s name.
   * @return The player’s name.
   */
  std::string getName () const;

  /**
   * Set the player’s name
   */
  void setName (const char* name);

  /**
   * Set the player’s name
   */
  void setName (std::string name);

  /**
   * Set the player’s coordinates.
   * @param The new coordinates of the player.
   */
  void setCoordinates (struct coordinates new_coords);

  /**
   * Get the player’s coordinates.
   */
  struct coordinates getCoordinates () const;

  /**
   * Set the default path to the save file.
   */
  void setSaveFilePath (std::string path);

  /**
   * Get the default path to the save file.
   */
  std::string getSaveFilePath () const;

  /**
   * Change the selected tool.
   * @param The direction we scroll : -1 for previous tool, 1 for next tool.
   */
  void changeSelectedTool (int scroll);

  /**
   * Returns the selected tool.
   */
  unsigned short getSelectedTool ();

  /**
   * Save the player’s data in a file.
   */
  int save (std::string path) const;

  /**
   * Save the player’s data in the default file.
   */
  int save () const;

  /**
   * Fill the members variables with those in the given save file.
   */
  int read_save (std::string path);

  /**
   * Fill the members variables with those in the default save file.
  */
  int read_save ();

  /**
   * Add an object to the inventory.
   */
  void add_items_to_inventory (int item_id, int number) ;

  void add_items_to_inventory (int item_id, int number,
                               int line, int column) ;

  /**
   * Remove an object from the inventory.
   */
  void remove_items_from_inventory (int item_id, int number) ;

  void remove_items_from_inventory (int item_id, int number,
                                    int line, int column) ;

  /**
   * Inform the object of the time passed.
   */
  void update_time (sf::Time elapsed_time);

  /**
   * Get the player’s inventory.
   */
  Inventory& get_inventory ();

private:
  std::string m_name;

  int m_health;

  /**
   * The unity is the number pixels per second.
   */
  int m_velocity;

  coordinates m_coordinates;

  Inventory m_inventory;
  std::array<std::pair<int,int>, 10> m_toolbar;

  unsigned short m_id;
  unsigned short m_selected_tool;

  std::string m_save_file_path;

  /**
   * Changes the player's coordinates by making him walk.
   */
  void player_walks (sf::Time elapsed_time);
};

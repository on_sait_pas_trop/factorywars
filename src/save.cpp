/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016-2018 Corentin Bocquillon <0x539@nybble.fr>
 * Copyright (C) 2016 Loup Fourment
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * Copyright (C) 2018 Cyril Colin <contact@ccolin.fr>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/
 *
 * @section DESCRIPTION
 *
 * map.cpp provide functions to read and write save files.
 */

#include <string>
#include <fstream>

#include <sys/stat.h>
#include <sys/types.h>

#include "save.hpp"
#include "structures.hpp"
#include "utils.h"

using namespace std;
using namespace Save;


string
Save::get_save_directory_path ()
{
  string save_directory_path;

  const char *xdg_data_home = getenv ("XDG_DATA_HOME");
  const char *home = getenv ("HOME");

  if (RUN_IN_PLACE)
    save_directory_path = "saves/";

  else if (xdg_data_home == NULL)
    {
      if (home == NULL)
        save_directory_path = "saves/";
      else
        {
          string directory_to_be_created = string (home) + "/.local/";

          if (!file_exists (directory_to_be_created))
            mkdir (directory_to_be_created.c_str (), 0755);

          directory_to_be_created += "share/";

          if (!file_exists (directory_to_be_created))
            mkdir (directory_to_be_created.c_str (), 0755);

          directory_to_be_created += "factorywars/";
          if (!file_exists (directory_to_be_created))
            mkdir (directory_to_be_created.c_str (), 0755);

          directory_to_be_created += "saves/";
          if (!file_exists (directory_to_be_created))
            mkdir (directory_to_be_created.c_str (), 0755);

          // On utilise la valeur par défaut de XDG_DATA_HOME
          save_directory_path = string (home) + "/.local/share/factorywars/saves/";
        }
    }

  else
    save_directory_path = string (xdg_data_home) + "/factorywars/saves/";

  return save_directory_path;
}


Chunk::Chunk (coordinates chunk_coordinates, shared_ptr<tile_proto> tile)
{
  // On initialise les coordonnées.
  m_chunk_coordinates = chunk_coordinates;
  m_tile_coordinates.x = tile->x ();
  m_tile_coordinates.y = tile->y ();

  for (int i = 0; i < tile->chunks_size (); i++)
    {
      if (tile->chunks (i).x () == chunk_coordinates.x
          && tile->chunks (i).y () == chunk_coordinates.y)
        {
          m_me = tile->mutable_chunks (i);
          break;
        }
    }

  update_last_use ();
}

inline coordinates
Chunk::getChunkCoordinates () const
{
  return m_chunk_coordinates;
}

inline coordinates
Chunk::getTileCoordinates () const
{
  return m_tile_coordinates;
}

void
Chunk::set_square_item (coordinates square_coordinates, int item_id,
                        int quantity)
{
  // On doit trouver le carré.
  for (int i = 0; i < m_me->squares_size (); i++)
    {
      if (m_me->squares (i).x () == square_coordinates.x
          && m_me->squares (i).y () == square_coordinates.y)
        {
          m_me->mutable_squares (i)->set_item (item_id);
          m_me->mutable_squares (i)->set_quantity (quantity);
          break;
        }
    }

  update_last_use ();
}

int
Chunk::get_square_item_id (coordinates square_coordinates)
{
  square_proto current_square;

  // On doit trouver le carré.
  for (int i = 0; i < m_me->squares_size (); i++)
    {
      current_square = m_me->squares (i);

      if (current_square.x () == square_coordinates.x
          && current_square.y () == square_coordinates.y)
        {
          update_last_use ();
          return (int) current_square.item ();
        }
    }

  return -1;
}

int
Chunk::get_square_item_quantity (coordinates square_coordinates)
{
  square_proto current_square;

  // On doit trouver le carré.
  for (int i = 0; i < m_me->squares_size (); i++)
    {
      current_square = m_me->squares (i);

      if (current_square.x () == square_coordinates.x
          && current_square.y () == square_coordinates.y)
        {
          update_last_use ();
          return (int) current_square.quantity ();
        }
    }

  return -1;
}

int
Chunk::get_ground_id (coordinates square_coordinates)
{
  update_last_use ();
  square_proto *current_square = get_square (square_coordinates);
  return (int) current_square->floor ();
}

bool
Chunk::operator== (const Chunk &other)
{
  if (m_chunk_coordinates.x == other.m_chunk_coordinates.x
      && m_chunk_coordinates.y == other.m_chunk_coordinates.y)
    return true;
  else
    return false;
}

time_t
Chunk::get_last_use ()
{
  return m_last_use;
}

void
Chunk::update_last_use ()
{
  m_last_use = time (NULL);
}

square_proto*
Chunk::get_square (coordinates coords)
{
  for (int i = 0; i < m_me->squares_size (); i++)
    {
      square_proto *current_square = m_me->mutable_squares (i);

      if (current_square->x () == coords.x
          && current_square->y () == coords.y)
        {
          return current_square;
        }
    }

  return nullptr;
}


Map::Map (string game_name, bool generate_seed)
{
  m_game_path = get_save_directory_path () + game_name;
  m_tiles_directory_path = m_game_path + "/tiles/";
  string seed_path = m_game_path + "/" + "seed";

  if (!file_exists (m_game_path))
    {
      if (!file_exists (get_save_directory_path ()))
        mkdir (get_save_directory_path ().c_str (), 0750);
      mkdir (m_game_path.c_str (), 0750);
      mkdir (m_tiles_directory_path.c_str (), 0750);
    }
  else
    {
      if (!file_exists (m_tiles_directory_path))
        {
          mkdir (m_tiles_directory_path.c_str (), 0755);
        }

      if (file_exists (seed_path))
        {
          ifstream seed_file (seed_path, ifstream::in);
          int seed;
          seed_file >> seed;
          seed_file.close ();

          m_map_generator.set_seed (seed);
        }

      else if (generate_seed)
        {
          srand (time (NULL));
          int seed = rand ();

          ofstream seed_file (seed_path, ofstream::out);
          seed_file << m_seed;
          seed_file << endl;
          seed_file.close ();

          m_map_generator.set_seed (seed);
        }
    }
}

Map::Map (string game_name, int seed)
{
  Map (game_name, false);
  m_map_generator.set_seed (seed);
}

int
Map::get_surface_item (struct coordinates chunk_coordinates,
                       struct coordinates square_coordinates)
{
  shared_ptr<Chunk> chunk = get_chunk_by_coordinates (chunk_coordinates);
  return chunk->get_square_item_id (square_coordinates);
}

void
Map::set_surface_item (struct coordinates chunk_coordinates,
                       struct coordinates square_coordinates,
                       int item_id,
                       int quantity)
{
  shared_ptr<Chunk> chunk = get_chunk_by_coordinates (chunk_coordinates);
  chunk->set_square_item (square_coordinates, item_id, quantity);
}

void
Map::save ()
{
  save_tiles ();
}

void
Map::load_chunk (coordinates chunk_coordinates)
{
  // Vérifier si le tronçon n’existe pas déjà.
  if (is_chunk_loaded (chunk_coordinates))
    return;

  // S’il n’existe pas, vérifier si la dalle est déjà en mémoire et si
  // elle ne l’est pas, la charger.
  coordinates tile_coordinates = get_tile_by_chunk (chunk_coordinates);
  load_tile (tile_coordinates);

  shared_ptr<tile_proto> tile = NULL;
  for (shared_ptr<tile_proto> current_tile : m_tiles)
    if (current_tile->x () == tile_coordinates.x
        && current_tile->y () == tile_coordinates.y)
      tile = current_tile;

  if (tile == NULL)
    return;

  for (int i = 0; i < tile->chunks_size (); i++)
    {
      if (tile->chunks (i).x () == chunk_coordinates.x
          && tile->chunks (i).y () == chunk_coordinates.y)
        {
          shared_ptr<Chunk> chunk = make_shared<Chunk> (chunk_coordinates, tile);
          m_chunks.push_back (chunk);
        }
    }
}

void
Map::load_tile (coordinates tile_coordinates)
{
  // Il faut vérifier que la dalle n’est pas déjà chargée.
  for (unsigned long i = 0; i < m_tiles.size (); i++)
    {
      if (m_tiles[i]->x () == tile_coordinates.x
          && m_tiles[i]->y () == tile_coordinates.y)
        return;
    }

  // On mémorise le chemin vers le fichier
  string tile_path = m_tiles_directory_path;
  tile_path += to_string (tile_coordinates.x) + ";";
  tile_path += to_string (tile_coordinates.y);

  if (!file_exists (tile_path))
    {
      generate_tile (tile_coordinates);
      load_tile (tile_coordinates);
      return;
    }

  // Il faut charger la dalle et la mettre à la fin du vecteur m_tiles.
  tile_proto tile;

  ifstream tile_file (tile_path, ios::binary);
  if (!tile_file)
    {
      cerr << "Error while opening the tile file." << endl;
      return;
    }
  else if (!tile.ParseFromIstream (&tile_file))
    {
      cerr << "Error while parsing the tile file." << endl;
      return;
    }
  tile_file.close ();

  m_tiles.push_back (shared_ptr<tile_proto> (new tile_proto (tile)));
}

bool
Map::is_chunk_loaded (coordinates chunk_coordinates)
{
  for (unsigned long i = 0; i < m_chunks.size (); i++)
    {
      if (m_chunks[i]->getChunkCoordinates ().x == chunk_coordinates.x
          && m_chunks[i]->getChunkCoordinates ().y == chunk_coordinates.y)
        return true;
    }
  return false;
}

void
Map::save_tile (shared_ptr<tile_proto> tile)
{
  coordinates tile_coordinates = {tile->x (), tile->y ()};

  string tile_file_path = m_tiles_directory_path;
  tile_file_path += to_string (tile_coordinates.x);
  tile_file_path += ";" + to_string (tile_coordinates.y);

  ofstream tile_file (tile_file_path, ios::binary | ios::trunc);
  if (!tile_file)
    {
      cerr << _("Error, the tile file cannot be opened.") << endl;
      return;
    }
  else if (!tile->SerializeToOstream (&tile_file))
    {
      cerr << _("Error, the tile cannot be saved.") << endl;
      return;
    }

  tile_file.close ();
}

void
Map::save_tiles ()
{
  for (unsigned i = 0; i < m_tiles.size (); i++)
    this->save_tile (m_tiles[i]);
}

void
Map::generate_tile (coordinates tile_coordinates)
{
  tile_proto tile = m_map_generator.generate_tile (tile_coordinates);

  // On l’écrit dans son fichier.
  string tile_file_path = m_tiles_directory_path;
  tile_file_path += to_string (tile_coordinates.x);
  tile_file_path += ";" + to_string (tile_coordinates.y);

  ofstream tile_file (tile_file_path, ios::trunc | ios::binary);
  if (!tile_file)
    {
      cerr << _("Error, the tile file cannot be opened.") << endl;
      return;
    }
  else if (!tile.SerializeToOstream (&tile_file))
    {
      cerr << _("Error, the tile file cannot be saved.") << endl;
      return;
    }

  tile_file.close ();
}

coordinates
Map::get_tile_by_chunk (coordinates chunk_coordinates) const
{
  coordinates tile_coordinates;

  tile_coordinates.x = chunk_coordinates.x / 8;
  tile_coordinates.y = chunk_coordinates.y / 8;

  return tile_coordinates;
}

shared_ptr<Chunk>
Map::get_chunk_by_coordinates (coordinates chunk_coordinates)
{
  for (shared_ptr<Chunk> chunk : m_chunks)
    {
      coordinates coords = chunk->getChunkCoordinates ();
      if (coords.x == chunk_coordinates.x && coords.y == chunk_coordinates.y)
        return chunk;
    }

  load_chunk (chunk_coordinates);
  return get_chunk_by_coordinates (chunk_coordinates);
}

void
Map::set_seed (int seed)
{
  m_map_generator.set_seed (seed);
}

void
Map::unload_unused_chunks ()
{
  time_t now = time (NULL);

  for (unsigned long i = 0; i < m_chunks.size (); i++)
    {
      time_t last_use = m_chunks[i]->get_last_use ();

      if (difftime (now, last_use) > 20.0)
        unload_chunk (m_chunks[i]);
    }
}

void
Map::unload_chunk (shared_ptr<Chunk> chunk)
{
  bool delete_tile = false;
  // On récupère les coordonnées de la dalle contenant le tronçon.
  coordinates tile_coordinates = chunk->getTileCoordinates ();

  // On le supprime.
  vector<shared_ptr<Chunk>>::iterator it = find (m_chunks.begin (), m_chunks.end (),
                                                 chunk);
  m_chunks.erase (it);

  // On regarde si d’autres tronçons contenus dans cette dalle existe
  // encore.
  delete_tile = true;
  for (unsigned long i = 0; i < m_chunks.size (); ++i)
    {
      if (m_chunks[i]->getTileCoordinates ().x == tile_coordinates.x
          && m_chunks[i]->getTileCoordinates ().y == tile_coordinates.y)
        {
          delete_tile = false;
          break;
        }
    }

  // S’il faut supprimer la dalle.
  if (delete_tile)
    {
      // On parcourt la liste des dalles.
      for (unsigned long i = 0; i < m_tiles.size (); i++)
        {
          // Si la dalle correspond.
          if (m_tiles[i]->x () == tile_coordinates.x
              && m_tiles[i]->y () == tile_coordinates.y)
            {
              // Il faut sauvegarder la dalle.
              save_tile (m_tiles[i]);

              // On libère et on détruit la dalle.
              m_tiles.erase (m_tiles.begin () + i);
            }
        }
    }
}

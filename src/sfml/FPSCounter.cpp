/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016 Corentin Bocquillon <0x539@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * FPSCounter.cpp is the FPS counter.
 */

#include <string>
#include <iostream>

#include "FPSCounter.h"
#include "../structures.hpp"

using namespace std;
using namespace Interface;

FPSCounter::FPSCounter (sf::RenderWindow & window, sf::Font font,
                        sf::Color colour)
  : Object (window), m_frames_passed (0), m_font (font), m_colour (colour)
{

}

void
FPSCounter::display ()
{
  ++m_frames_passed;
  if (m_clock.getElapsedTime ().asMilliseconds () > 500)
    {
      m_clock.restart ();
      m_current_fps = m_frames_passed;
      m_frames_passed = 0;
    }

  string counter = to_string (m_current_fps * 2) + " fps";
  sf::Text text (counter, m_font, 18);
  text.setFillColor (m_colour);
  m_window.draw (text);
}

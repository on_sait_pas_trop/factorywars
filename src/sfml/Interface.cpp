/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016 Corentin Bocquillon <0x539@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/
 *
 * @section DESCRIPTION
 *
 * Interface.cpp is for the interface and objects of the interface.
 */

#include <algorithm>
#include <utility>

#include "Interface.h"
#include "gui_utils.h"

using namespace std;

namespace Interface
{
  Object::Object (sf::RenderWindow & window)
    : m_window (window)
  {

  }

  Object::~Object ()
  {

  }

  void
  Object::notify_elapsed_time (unsigned elapsed_time)
  {

  }

  SimpleObject::SimpleObject (sf::RenderWindow & window,
                              sf::Texture texture,
                              float scale,
                              float horizontal_spacing,
                              float vertical_spacing,
                              int horizontal_offset,
                              int vertical_offset)
    : Object (window), m_texture (texture),
      m_horizontal_spacing (horizontal_spacing),
      m_vertical_spacing (vertical_spacing),
      m_horizontal_offset (horizontal_offset),
      m_vertical_offset (vertical_offset)
  {
    m_sprite.setTexture (m_texture, true);
    m_sprite.setScale (scale, scale);

    sf::Vector2f position;
    sf::Vector2u screen_size = m_window.getSize ();
    sf::FloatRect sprite_rect = m_sprite.getGlobalBounds ();
    sf::Vector2f sprite_size (sprite_rect.width, sprite_rect.height);

    position.x = m_horizontal_spacing * screen_size.x - sprite_size.x / 2
      + m_horizontal_offset;
    position.y = m_vertical_spacing * screen_size.y - sprite_size.y / 2
      + m_vertical_offset;
    m_sprite.setPosition (position);
  }

  void
  SimpleObject::display ()
  {
    m_window.draw (m_sprite);
  }

  Interface::Interface (sf::RenderWindow & window)
    : m_window (window)
  {

  }

  void
  Interface::add_object (shared_ptr<Object> object)
  {
    m_objects.push_back (object);
  }

  void
  Interface::remove_object (shared_ptr<Object> &object)
  {
    vector<shared_ptr<Object>>::iterator it;
    it = find (m_objects.begin (), m_objects.end (), object);
    if (it != m_objects.end ())
      m_objects.erase (it);
  }

  void
  Interface::notify_elapsed_time (unsigned elapsed_time)
  {
    for_each (m_objects.begin (), m_objects.end (),
              [elapsed_time] (shared_ptr<Object> & object)
              { object->notify_elapsed_time (elapsed_time); });
  }

  void
  Interface::display ()
  {
    for (auto &object : m_objects)
      object->display ();
  }
}

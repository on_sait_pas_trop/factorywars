/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016-2018 Corentin Bocquillon <0x539@nybble.fr>
 * Copyright (C) 2016 Loup Fourment
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/
 *
 * @section DESCRIPTION
 *
 * Interface.h is the header of Interface.cpp.
 */

#pragma once

#include <vector>
#include <memory>
#include <SFML/Graphics.hpp>

#include "../structures.hpp"
#include "gui_utils.h"

namespace Interface
{
  class Object
  {
  public:
    Object (sf::RenderWindow & window);
    virtual ~Object ();

    virtual void display () = 0;

    virtual void notify_elapsed_time (unsigned elapsed_time);

  protected:
    sf::RenderWindow & m_window;
  };

  class SimpleObject : public Object
  {
  public:
    SimpleObject (sf::RenderWindow & window, sf::Texture texture,
                  float scale, float horizontal_spacing,
                  float vertical_spacing, int horizontal_offset,
                  int vertical_offset);

    virtual void display ();

  private:
    sf::Texture m_texture;
    sf::Sprite m_sprite;
    float m_horizontal_spacing;
    float m_vertical_spacing;
    int m_horizontal_offset;
    int m_vertical_offset;
  };

  class Interface
  {
  public:
    Interface (sf::RenderWindow & window);
    void add_object (std::shared_ptr<Object> object);
    void remove_object (std::shared_ptr<Object> &object);
    void notify_elapsed_time (unsigned elapsed_time);
    void display ();

  private:
    sf::RenderWindow & m_window;
    std::vector<std::shared_ptr<Object>> m_objects;
  };
}

/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016 Corentin Bocquillon <corentin@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * Copyright (C) 2018 Cyril Colin <contact@ccolin.fr>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * display_map.cpp contains the functions to display the map.
 */

#include "display_map.h"

void
display_background (sf::RenderWindow &window,
                    Graphics::Map &map,
                    struct coordinates screen_origin)
{
  const int chunk_width = Save::square_per_side * Save::square_size;

  struct coordinates coords;
  struct size screen_dimensions = {.x = (int) window.getSize ().x,
                                   .y = (int) window.getSize ().y};

  for(int i(0); i < screen_dimensions.y + screen_origin.y % chunk_width; i += chunk_width)
    {
      for(int j(0); j < screen_dimensions.x + screen_origin.x % chunk_width ; j += chunk_width)
        {
          coordinates offset = {.x = (int) screen_origin.x + j,
                                .y = (int) screen_origin.y + i};
          coords = get_chunk_coordinates_from_offset (offset);

          Graphics::Chunk &chunk = map.get_chunk (coords);
          chunk.move (j - screen_origin.x % chunk_width, i - screen_origin.y % chunk_width);
          window.draw (chunk);
          chunk.setPosition (0, 0);
        }
    }
}

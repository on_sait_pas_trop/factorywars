/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016 Corentin Bocquillon <0x539@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * game.cpp contains functions used only to start a game.
 */

#include <vector>
#include <map>
#include <string>
#include <SFML/Window.hpp>

#include "game.h"
#include "../save.hpp"

using namespace std;

int
run_gui ()
{
  Configuration configuration;

  unsigned height = stoi (configuration.get_config_value ("height"));
  unsigned width = stoi (configuration.get_config_value ("width"));

  sf::RenderWindow window (sf::VideoMode (width, height), "Factorywars");
  window.setVerticalSyncEnabled (true);

  int stay = 1;
  while (stay)
    {
      Menu::MainMenu main_menu (window);
      main_menu.display ();
      Menu::MainMenuChoice choice = main_menu.get_choice ();
      if (choice == Menu::MainMenuChoice::quit)
        return 0;

      string save_path = main_menu.get_save_path ();
      Game game (window, save_path, configuration.get_config_value ("name"));
      int ret = game.run ();

      if (ret == 0)
	stay = 0;
    }

  return 0;
}

void
display_players (vector<Player>& players, struct coordinates screen_origin,
		 sf::RenderWindow & window, sf::Texture player_texture)
{
  struct coordinates player_coordinates;

  for (Player player : players)
    {
      player_coordinates = player.getCoordinates ();

      if (player_coordinates.x >= screen_origin.x
	  && player_coordinates.x <= screen_origin.x
          + (int) window.getSize ().x)
	{
	  if (player_coordinates.y >= screen_origin.y
	      && player_coordinates.y <= screen_origin.y
              + (int) window.getSize ().y)
	    {
	      player_coordinates.x -= screen_origin.x;
	      player_coordinates.y -= screen_origin.y;

              sf::Sprite player_sprite (player_texture);
              player_sprite.setPosition (player_coordinates.x,
                                         player_coordinates.y);
              player_sprite.setScale (0.05, 0.05);

	      window.draw (player_sprite);
	    }
	}
    }
}

void
save_players (vector<Player>& players, string player_save_directory)
{
  for (Player player : players)
    {
      if (player.getName ().empty ())
	continue;

      if (player.getSaveFilePath ().empty ())
	player.setSaveFilePath (player_save_directory);

      player.save ();
    }
}

 Game::Game (sf::RenderWindow &window, string save_name,
             string player_name)
  : m_window (window), m_interface (window), m_in_game_menu (window),
    m_quit (false), m_save (false), m_map (save_name),
    m_inventory_interface (window), m_inventory_opened (false)
{
  load_textures ();

  m_save_path = Save::get_save_directory_path () + save_name + "/";
  create_player (player_name);

  calculate_screen_relative_player_coordinates ();
  calculate_screen_origin ();
  calculate_screen_center ();
  set_event_handlers ();

  // Chargement de la police.
  string font_path;
  if (!RUN_IN_PLACE)
    font_path = FONTSDIR"/FreeSans.ttf";
  else
    font_path = "../media/fonts/FreeSans.ttf";

  if (!m_ttf_freesans.loadFromFile (font_path))
    throw runtime_error (font_path + " " + _("does not exist"));

  m_inventory_interface.set_font (m_ttf_freesans);

  m_current_event_handler = &m_default_event_handler;
  create_event_handlers ();
  create_interface ();
}

Game::~Game ()
{

}

int
Game::run ()
{
  m_window.setKeyRepeatEnabled (false);
  display_a_frame ();

  int r = 1;
  sf::Clock last_displayed_frame_time;
  while (true)
    {
      this->handle_events ();
      save_if_necessary ();
      if (m_quit)
        {
          r = 0;
          break;
        }

      move_player_if_necessary ();
      handle_clicks ();
      display_a_frame ();

      update_time (last_displayed_frame_time.getElapsedTime ());
      last_displayed_frame_time.restart ();
    }

  m_window.setKeyRepeatEnabled (true);
  return r;
}

void
Game::create_player (string player_name)
{
  m_players.push_back (Player ());
  m_players[0].setSaveFilePath (m_save_path + player_name);
  m_players[0].read_save ();
  m_current_player_texture = m_player_textures[1];
  m_inventory_interface.set_inventory (m_players[0].get_inventory ());
}

void
Game::load_textures ()
{
  vector<string> player_textures_paths;
  vector<string> hud_textures_paths;

  if (!RUN_IN_PLACE)
    {
      player_textures_paths.push_back (TEXTURESDIR"/LEFT.png");
      player_textures_paths.push_back (TEXTURESDIR"/RIGHT.png");
      player_textures_paths.push_back (TEXTURESDIR"/LEFT.png");
      player_textures_paths.push_back (TEXTURESDIR"/RIGHT.png");

      hud_textures_paths.push_back (TEXTURESDIR"/toolbar.png");
    }
  else
    {
      player_textures_paths.push_back ("../media/textures/LEFT.png");
      player_textures_paths.push_back ("../media/textures/RIGHT.png");
      player_textures_paths.push_back ("../media/textures/LEFT.png");
      player_textures_paths.push_back ("../media/textures/RIGHT.png");

      hud_textures_paths.push_back ("../media/hud/toolbar.png");
    }

  for (string path : player_textures_paths)
    {
      sf::Texture texture;
      texture.loadFromFile (path);
      m_player_textures.push_back (texture);
    }

  for (string path : hud_textures_paths)
    {
      sf::Texture texture;
      texture.loadFromFile (path);
      m_hud_textures.push_back (texture);
    }
}

void
Game::calculate_screen_relative_player_coordinates ()
{
  if (m_players[0].getCoordinates ().x > m_screen_center.x)
    m_screen_relative_player_coordinates.x = m_screen_center.x;
  else
    m_screen_relative_player_coordinates.x = m_players[0].getCoordinates ().x;

  if (m_players[0].getCoordinates ().y > m_screen_center.y)
    m_screen_relative_player_coordinates.y = m_screen_center.y;
  else
    m_screen_relative_player_coordinates.y = m_players[0].getCoordinates ().y;
}

void
Game::calculate_screen_origin ()
{
  if (m_screen_relative_player_coordinates.x == m_screen_center.x)
    m_screen_origin.x = m_players[0].getCoordinates ().x
      - m_screen_relative_player_coordinates.x;
  else
    m_screen_origin.x = 0;

  if (m_screen_relative_player_coordinates.y == m_screen_center.y)
    m_screen_origin.y = m_players[0].getCoordinates ().y
      - m_screen_relative_player_coordinates.y;
  else
    m_screen_origin.y = 0;
}

void
Game::calculate_screen_center ()
{
  m_screen_center.x = m_window.getSize ().x / 2;
  m_screen_center.y = m_window.getSize ().y / 2;
}

void
Game::set_event_handlers ()
{
  m_keyboard.set_event_handler
    (sf::Keyboard::Key::Left, true,
     [&, this] () mutable
     {
       this->m_current_player_texture = this->m_player_textures[0];
     });

  m_keyboard.set_event_handler
    (sf::Keyboard::Key::Right, true,
     [&, this] () mutable
     {
       this->m_current_player_texture = this->m_player_textures[1];
     });

  m_keyboard.set_event_handler
    (sf::Keyboard::Key::Escape, true,
     [&, this] () mutable
     {
       if (this->m_inventory_opened)
         this->close_inventory ();
       else
         this->display_menu ();
     });

  m_mouse.set_event_handler
    (sf::Mouse::Button::Left, true,
     [&, this] (coordinates click_coords) mutable
     {
       map_coordinates click_map_coords
         = get_map_coords (click_coords,
                           this->m_screen_origin);

       // cout << click_map_coords.chunk.x << ";" << click_map_coords.chunk.y << " " << click_map_coords.square.x << ";" << click_map_coords.square.y << endl;
       if (this->m_map.get_save ().get_surface_item
           (click_map_coords.chunk, click_map_coords.square) == -1)
         {
           this->m_map.set_surface_item (click_map_coords.chunk,
                                         click_map_coords.square, 2);
         }
     });

  m_mouse.set_event_handler
    (sf::Mouse::Button::Right, true,
     [&, this] (coordinates click_coords) mutable
     {
       map_coordinates click_map_coords
         = get_map_coords (click_coords,
                           this->m_screen_origin);

       if (this->m_map.get_save ().get_surface_item
           (click_map_coords.chunk, click_map_coords.square) != -1)
         {
           this->m_map.set_surface_item
             (click_map_coords.chunk, click_map_coords.square, -1, 0);
         }
     });

  m_keyboard.set_event_handler
    (sf::Keyboard::Key::E, false,
     [this] ()
     {
       if (this->m_inventory_opened)
         close_inventory ();
       else
         open_inventory ();
     });
}

void
Game::create_event_handlers ()
{
  m_default_event_handler
    = [this] (sf::Event event)
      {
        coordinates click_coords;

        switch (event.type)
          {
          case sf::Event::Closed:
            this->m_quit = true;
            break;

          case sf::Event::KeyPressed:
            this->m_keyboard.handle_keyboard_event (event.key.code, true);
            break;

          case sf::Event::KeyReleased:
            this->m_keyboard.handle_keyboard_event (event.key.code, false);
            break;

          case sf::Event::MouseButtonPressed:
            click_coords = {.x = event.mouseButton.x,
                            .y = event.mouseButton.y};
            this->m_mouse.handle_mouse_event (event.mouseButton.button, true,
                                              click_coords);
            break;

          case sf::Event::MouseButtonReleased:
            click_coords = {.x = event.mouseButton.x,
                            .y = event.mouseButton.y};
            this->m_mouse.handle_mouse_event (event.mouseButton.button, false,
                                              click_coords);
            break;

          case sf::Event::MouseWheelScrolled:
            handle_mousewheel (event.mouseWheelScroll, this->m_players);
            break;

          case sf::Event::Resized:
            this->calculate_screen_center ();
            this->calculate_screen_relative_player_coordinates ();
            this->calculate_screen_origin ();
            break;

          default:
            break;
          }
      };

  m_inventory_event_handler
    = [this] (sf::Event event)
      {
        if (event.type == sf::Event::KeyReleased
            && (event.key.code == sf::Keyboard::Key::Escape
                || event.key.code == sf::Keyboard::Key::E))
          {
            this->close_inventory ();
            return;
          }

        switch (event.type)
          {
          case sf::Event::Resized:
          case sf::Event::Closed:
            this->m_default_event_handler (event);
            break;

          default:
            this->m_inventory_interface.handle_event (event);
            break;
          }
      };
}

void
Game::create_interface ()
{
  // Ajout de la barre d’outil.
  m_interface.add_object (make_shared<Interface::SimpleObject>
                          (m_window, m_hud_textures[0], 0.5, 0.5, 1.0, 0, -28));

  // Ajout du compteur d’IPS.
  m_interface.add_object (make_shared<Interface::FPSCounter>
                          (m_window, m_ttf_freesans, sf::Color::White));
}

void
Game::display_a_frame ()
{
  display_background (m_window, m_map, m_screen_origin);
  m_interface.display ();
  display_players (m_players, m_screen_origin, m_window,
                   m_current_player_texture);
  if (m_inventory_opened)
    m_inventory_interface.display ();
  m_window.display ();
}

void
Game::update_time (sf::Time elapsed_time)
{
  m_players[0].update_time (elapsed_time);
}

void
Game::handle_events ()
{
  sf::Event event;

  struct coordinates click_coords;
  while (m_window.pollEvent (event))
    {
      (*m_current_event_handler) (event);
    }
}

void
Game::display_menu ()
{
  m_in_game_menu.display ();
  Menu::InGameMenuChoice choice = m_in_game_menu.get_choice ();
  m_in_game_menu.reset_choice ();
  if (choice == Menu::InGameMenuChoice::quit)
    m_quit = true;
  else if (choice == Menu::InGameMenuChoice::save)
    m_save = true;
  else if (choice == Menu::InGameMenuChoice::save_quit)
    {
      m_quit = true;
      m_save = true;
    }
}

void
Game::save_if_necessary ()
{
  if (!m_save)
    return;
  else
    m_save = false;

  m_map.get_save ().save ();

  for (Player & player : m_players)
    {
      if (player.getName ().empty ())
	continue;

      if (player.getSaveFilePath ().empty ())
	player.setSaveFilePath (m_save_path);

      player.save ();
    }

}

void
Game::move_player_if_necessary ()
{
  if (m_players[0].getCoordinates ().x > m_screen_center.x)
    m_screen_origin.x = m_players[0].getCoordinates ().x - m_screen_center.x;
  else
    m_screen_origin.x = 0;

  if (m_players[0].getCoordinates ().y > m_screen_center.y)
    m_screen_origin.y = m_players[0].getCoordinates ().y - m_screen_center.y;
  else
    m_screen_origin.y = 0;
}

void
Game::handle_clicks ()
{

}

void
Game::open_inventory ()
{
  m_current_event_handler = &m_inventory_event_handler;
  m_inventory_opened = true;
}

void
Game::close_inventory ()
{
  m_current_event_handler = &m_default_event_handler;
  m_inventory_opened = false;
}

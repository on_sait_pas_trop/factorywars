/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016 Corentin Bocquillon <0x539@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * game.h is the header of game.cpp.
 */

#pragma once

#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include <exception>
#include <stdexcept>

#include "gui_utils.h"
#include "../player.h"
#include "menu.h"
#include "map.hpp"
#include "display_map.h"
#include "game_events.h"
#include "../config.h"
#include "../utils.h"
#include "FPSCounter.h"
#include "keyboard.h"
#include "mouse.h"
#include "Interface.h"
#include "inventory_interface.hpp"


/**
 * Run the gui
 * @return -1 if there is an error or 0 if there is no error.
 */
int run_gui ();

/**
 * Blit the players in “players”.
 *
 * @param players is the vector containing every players.
 * @param screen_origin is the offset in pixels from the origin of the map.
 * @param renderer is the renderer where we blit the players.
 * @param player_texture is the texture to blit.
 * @param screen_dimensions is the screen’s dimensions.
2 */
void display_players (std::vector<Player>& players,
		      struct coordinates screen_origin,
		      sf::RenderWindow & window,
		      sf::Texture player_texture);

/**
 * Save the players
 */
void save_players (std::vector<Player>& players,
                   std::string save_dir_path);

class Game
{
 public:
  Game (sf::RenderWindow & window, std::string save_name,
        std::string player_name);
  ~Game ();
  int run ();

 private:
  // Textures.
  std::vector<sf::Texture> m_player_textures;
  sf::Texture m_current_player_texture;
  std::vector<sf::Texture> m_hud_textures;

  sf::RenderWindow &m_window;
  std::string m_save_path;
  std::vector<Player> m_players;

  Interface::Interface m_interface;
  Menu::InGameMenu m_in_game_menu;

  bool m_quit;
  bool m_save;

  // The map.
  Graphics::Map m_map;

  // Inputs
  Keyboard m_keyboard;
  Mouse m_mouse;

  coordinates m_screen_relative_player_coordinates;
  coordinates m_screen_origin;
  coordinates m_screen_center;

  // Font
  sf::Font m_ttf_freesans;

  std::function<void (sf::Event)> *m_current_event_handler;
  std::function<void (sf::Event)> m_default_event_handler;
  std::function<void (sf::Event)> m_inventory_event_handler;

  Graphics::InventoryInterface m_inventory_interface;

  bool m_inventory_opened;

  void create_player (std::string player_name);
  void create_interface ();

  void load_textures ();

  void calculate_screen_relative_player_coordinates ();
  void calculate_screen_origin ();
  void calculate_screen_center ();

  // Events
  void set_event_handlers ();
  void create_event_handlers ();
  void handle_events ();
  void display_menu ();
  void save_if_necessary ();
  void move_player_if_necessary ();
  void handle_clicks ();

  void display_a_frame ();
  void update_time (sf::Time elapsed_time);

  void open_inventory ();
  void close_inventory ();
};

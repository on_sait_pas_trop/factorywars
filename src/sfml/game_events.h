/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016 Corentin Bocquillon <corentin@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * game_events.h is the header of game_events.cpp.
 */

#pragma once

#include <vector>
#include <SFML/Graphics.hpp>

#include "../structures.hpp"
#include "../player.h"
#include "gui_utils.h"

/**
 * Handle the mousewheel events.
 */
int handle_mousewheel (sf::Event::MouseWheelScrollEvent wheel_event,
		       std::vector<Player>& players);

/**
 * Get the chunk coordinates and the square coordinates where the player
 * clicked.
 *
 * @param click_coords is the coordinates relative to the origin of the screen
 * where the player clicked.
 * @param screen_origin is the offset in pixels from the origin of the map.
 */
struct map_coordinates get_map_coords (coordinates click_coords,
				       struct coordinates screen_origin);

/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2018 Corentin Bocquillon <0x539@nybble.fr>
 * Copyright (C) 2016 Loup Fourment
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * ground.cpp gives accessors for grounds textures.
 */

#include <map>
#include <string>
#include <SFML/Graphics.hpp>
#include <vector>

#include "ground.h"

using namespace std;

std::map<int,sf::Texture> GroundsTexture::m_ground_textures_map;

GroundsTexture::GroundsTexture ()
{
  if (m_ground_textures_map.size () == 0)
    load_textures ();
}

GroundsTexture::~GroundsTexture ()
{

}

sf::Texture&
GroundsTexture::get_ground_texture (int ground_id)
{
  return m_ground_textures_map[ground_id];
}

void
GroundsTexture::load_textures ()
{
  string prefix;
  if (!RUN_IN_PLACE)
    prefix = string (TEXTURESDIR) + "/";
  else
    prefix = "../media/textures/";

  vector<string> m_ground_textures_path;

  m_ground_textures_path.push_back (prefix + "biome1.png");
  m_ground_textures_path.push_back (prefix + "biome2.png");
  m_ground_textures_path.push_back (prefix + "biome3.png");

  for (unsigned i = 1; i < m_ground_textures_path.size () + 1; ++i)
    {
      if (!m_ground_textures_map[i].loadFromFile (m_ground_textures_path[i-1]))
        throw runtime_error (m_ground_textures_path[i] + string (" ")
                             + "does not exist");
    }
}

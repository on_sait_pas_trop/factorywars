/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016 Corentin Bocquillon <0x539@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * gui_utils.cpp contains the gui's utils.
 */

#include <iostream>

#include "gui_utils.h"

using namespace std;

Rectangle::Rectangle (int _x, int _y, int _w, int _h)
  : x (_x), y (_y), w (_w), h (_h)
{

}

Rectangle::Rectangle (const Rectangle& other)
{
  copy (other);
}

Rectangle::Rectangle (sf::IntRect other)
{
  x = other.left;
  y = other.top;
  w = other.width;
  h = other.height;
}

Rectangle::Rectangle (sf::FloatRect other)
{
  x = other.left;
  y = other.top;
  w = other.width;
  h = other.height;
}

Rectangle::operator sf::IntRect () const
{
  return sf::IntRect (x, y, w, h);
}

Rectangle::operator sf::FloatRect () const
{
  return sf::FloatRect (x, y, w, h);
}

Rectangle&
Rectangle::operator= (const Rectangle& other)
{
  copy (other);
  return *this;
}

bool
Rectangle::operator== (const Rectangle& other) const
{
  return other.x == x &&  y == other.y && w == other.w
    && h == other.h;
}

bool
Rectangle::operator!= (const Rectangle& other) const
{
  return !(*this == other);
}

bool
Rectangle::operator< (const Rectangle& other) const
{
  return w * h < other.w * other.h;
}

bool
Rectangle::operator<= (const Rectangle& other) const
{
  return *this < other || *this == other;
}

bool
Rectangle::operator> (const Rectangle& other) const
{
  return !(*this <= other);
}

bool
Rectangle::operator>= (const Rectangle& other) const
{
  return *this > other || *this == other;
}

void
Rectangle::copy (const Rectangle& other)
{
  x = other.x;
  y = other.y;
  w = other.w;
  h = other.h;
}

std::ostream&
operator<< (std::ostream& stream, Rectangle rectangle)
{
  return stream << rectangle.x << ";" << rectangle.y << " "
                << rectangle.w << " width " << rectangle.h << " height";
}

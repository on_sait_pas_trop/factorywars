/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016 Corentin Bocquillon <corentin@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * gui_utils.h is the header of gui_utils.cpp
 */

#pragma once

#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>

#include "../structures.hpp"
#include "../config.h"

#include "../gettext.h"
#define _(string) gettext (string)

// Enumeration to use with keys_state
enum KEYBOARD_BUTTONS
  {
    key_up,
    key_down,
    key_left,
    key_right,
    key_escape
  };

// Enumeration to use with clicks_state
enum MOUSE_BUTTONS
  {
    click_left,
    click_middle,
    click_right,
    click_x1,
    click_x2
  };

template <typename T>
void center_rectangle (const sf::Rect<T> container, sf::Rect<T> & content)
{
  content.left = container.left;
  content.top = container.top;
  if (content.width <= container.width) // Si le texte n’est pas trop large.
    content.left += (container.width - content.width) / 2;
  if (content.height <= container.height) // Si le texte n’est pas trop haut.
    content.top += (container.height - content.height) / 2;
}

template <typename T>
void center_rectangles_vertically (const sf::Rect<T> container,
                                   std::vector<sf::Rect<T>> & contents) throw ()
{
  if (contents.size () <= 0)
    throw std::invalid_argument ("at least one rectangle is needed");

  T contents_height_sum = 0;
  for (sf::Rect<T> & rectangle : contents)
    contents_height_sum += rectangle.height;
  if (contents_height_sum > container.height)
    throw std::invalid_argument ("contents height sum is too high");

  T inter_rectangles_space = container.height - contents_height_sum;
  inter_rectangles_space /= contents.size () + 1;

  // Il nous faut la hauteur moyenne d’un rectangle.
  T average_rectangle_height = contents_height_sum / contents.size ();

  // Il faut prendre la largeur du rectangle le plus large.
  T widest_rectangle_width = 0;
  for (sf::Rect<T> & rectangle : contents)
    {
      if (widest_rectangle_width < rectangle.width)
        widest_rectangle_width = rectangle.width;
    };
  if (widest_rectangle_width > container.width)
    throw std::invalid_argument ("a rectangle is too wide");

  T side_space = container.width - widest_rectangle_width;
  side_space /= 2;

  // Le premier bouton est un cas particulier.
  contents[0].left = side_space + container.left;
  contents[0].top = inter_rectangles_space + container.top;

  for (unsigned i = 1; i < contents.size (); ++i)
    {
      contents[i].top = contents[i - 1].top;
      contents[i].top += average_rectangle_height;
      contents[i].top += inter_rectangles_space;

      contents[i].left = side_space + container.left;
    }
}

class Rectangle
{
 public:
  Rectangle (int _x = 0, int _y = 0, int _w = 0, int _h = 0);
  Rectangle (const Rectangle& other);
  Rectangle (sf::IntRect other);
  Rectangle (sf::FloatRect other);
  operator sf::IntRect () const;
  operator sf::FloatRect () const;
  Rectangle& operator= (const Rectangle& other);
  bool operator== (const Rectangle& other) const;
  bool operator!= (const Rectangle& other) const;
  bool operator< (const Rectangle& other) const;
  bool operator<= (const Rectangle& other) const;
  bool operator> (const Rectangle& other) const;
  bool operator>= (const Rectangle& other) const;

  int x, y, w, h;

 private:
  void copy (const Rectangle& other);
};

std::ostream& operator<< (std::ostream& stream, Rectangle rectangle);

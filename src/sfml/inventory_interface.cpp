/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @section LICENSE
 *
 * Copyright (C) 2018 Corentin Bocquillon <0x539@nybble.fr>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * Displaying an inventory interface.
 */

#include "inventory_interface.hpp"

using namespace std;
using namespace Graphics;

InventoryInterface::InventoryInterface (sf::RenderWindow &window)
  : m_window (window), m_inventory (m_null_inventory)
{
  if (!RUN_IN_PLACE)
    m_inventory_texture.loadFromFile (TEXTURESDIR"/inventaire.png");
  else
    m_inventory_texture.loadFromFile ("../media/hud/inventaire.png");

  m_inventory_sprite.setTexture (m_inventory_texture);
}

void
InventoryInterface::set_inventory (Inventory &inventory)
{
  m_inventory = inventory;
}

void
InventoryInterface::set_font (sf::Font font)
{
  m_font = font;
}

void
InventoryInterface::display ()
{
  // Display the inventory texture.
  sf::Vector2u window_size = m_window.getSize ();
  sf::Vector2u inventory_texture_size = m_inventory_texture.getSize ();
  sf::Vector2f inventory_position (window_size.x - inventory_texture_size.x / 2
                                   - (window_size.x / 2),
                                   window_size.y - inventory_texture_size.y / 2
                                   - (window_size.y / 2));
  m_inventory_sprite.setPosition (inventory_position);
  m_window.draw (m_inventory_sprite);

  // Display text.
  sf::Text inventory_title (_("Inventory"), m_font, 20);
  sf::FloatRect inventory_title_rect = inventory_title.getLocalBounds ();
  inventory_title.setPosition (inventory_position.x + 10,
                               inventory_position.y +
                               (48 - inventory_title_rect.height) / 2);
  m_window.draw (inventory_title);

  // Display the items.
}

void
InventoryInterface::handle_event (sf::Event event)
{
  switch (event.type)
    {
    default:
      break;
    }
}

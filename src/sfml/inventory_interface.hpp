/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 *
 * @section LICENSE
 *
 * Copyright (C) 2018 Corentin Bocquillon <0x539@nybble.fr>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * Displaying an inventory interface.
 */

#pragma once

#include <SFML/Graphics.hpp>
#include "../player.h"

namespace Graphics
{
  class InventoryInterface
  {
  public:
    InventoryInterface (sf::RenderWindow &window);

    void set_inventory (Inventory &inventory);
    void set_font (sf::Font font);

    void display ();
    void handle_event (sf::Event event);

  private:
    sf::RenderWindow &m_window;
    sf::Texture m_inventory_texture;
    sf::Sprite m_inventory_sprite;
    Inventory m_null_inventory;
    sf::Font m_font;
    Inventory &m_inventory;
  };
}

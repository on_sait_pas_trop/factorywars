/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016-2018 Corentin Bocquillon <0x539@nybble.fr>
 * Copyright (C) 2016 Loup Fourment
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * Copyright (C) 2018 Cyril Colin <contact@ccolin.fr>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/
 *
 * @section DESCRIPTION
 *
 * map.cpp provide functions to obtain map’s textures.
 */

#include <string>
#include <vector>
#include <utility>
#include <json/json.h>
#include <fstream>
#include <stdexcept>

#include "map.hpp"
#include "save.hpp"

using namespace std;
using namespace Graphics;

Chunk::Chunk (shared_ptr<Save::Chunk> me, shared_ptr<sf::Texture> items,
              shared_ptr<sf::Texture> grounds)
  : m_items (items), m_grounds (grounds), m_me (me)
{

}

coordinates
Chunk::get_coordinates () const
{
  return m_me->getChunkCoordinates ();
}

shared_ptr<Save::Chunk>
Chunk::get_save_chunk ()
{
  return m_me;
}

void
Chunk::set_vertices (sf::VertexArray ground_vertices,
                     vector<sf::VertexArray> surface_vertices)
{
  m_ground_vertices = ground_vertices;
  m_surface_vertices = surface_vertices;
}

void
Chunk::set_save_chunk (std::shared_ptr<Save::Chunk> chunk)
{
  m_me = chunk;
}

Chunk&
Chunk::operator= (const Chunk &other)
{
  m_ground_vertices = other.m_ground_vertices;
  m_surface_vertices = other.m_surface_vertices;
  m_items = other.m_items;
  m_grounds = other.m_grounds;
  m_me = other.m_me;

  return *this;
}

void
Chunk::draw (sf::RenderTarget& target, sf::RenderStates states) const
{
  states.transform *= getTransform();
  states.texture = m_grounds.get ();
  target.draw(m_ground_vertices, states);

  states.texture = m_items.get ();
  for (unsigned i = 0; i < m_surface_vertices.size (); ++i)
    target.draw(m_surface_vertices[i], states);
}

Map::Map (string game_name)
  : m_save (game_name)
{
  m_items_json = load_json_document ("../media/items-spritesheet.json");
  m_grounds_json = load_json_document ("../media/grounds-spritesheet.json");

  if (RUN_IN_PLACE)
    {
      m_items.loadFromFile ("../media/items-spritesheet.png");
      m_grounds.loadFromFile ("../media/grounds-spritesheet.png");
    }
  else
    {
      m_items.loadFromFile (TEXTURESDIR"items-spritesheet.png");
      m_grounds.loadFromFile (TEXTURESDIR"grounds-spritesheet.png");
    }

  set_item_max_height ();
}

Map::Map (string game_name, int seed)
  : Map (game_name)
{
  m_save.set_seed (seed);
}

Map::~Map ()
{
  m_save.save ();
}

Chunk&
Map::get_chunk (coordinates coords)
{
  for (Chunk &chunk : m_chunks)
    {
      coordinates chunk_coordinates = chunk.get_coordinates ();
      if (chunk_coordinates.x == coords.x
          && chunk_coordinates.y == coords.y)
        return chunk;
    }

  Chunk chunk (m_save.get_chunk_by_coordinates (coords),
               make_shared<sf::Texture> (m_items),
               make_shared<sf::Texture> (m_grounds));
  update_chunk_vertices (chunk);
  m_chunks.push_back (chunk);

  return get_chunk (coords);
}

Save::Map&
Map::get_save ()
{
  return m_save;
}

void
Map::set_surface_item (coordinates chunk_coordinates,
                       coordinates square_coordinates, int item_id,
                       int quantity)
{
  m_save.set_surface_item (chunk_coordinates, square_coordinates,
                           item_id, quantity);

  update_chunk_vertices (get_chunk (chunk_coordinates));
  coordinates coordinates = chunk_coordinates;
  coordinates.y -= 1;
  update_chunk_vertices (get_chunk (coordinates));
}

void
Map::set_vertices_position (sf::VertexArray &vertex_array,
                            const unsigned &first_position,
                            const coordinates &square_coordinates)
{
  vector<sf::Vector2f> positions;
  positions.push_back (sf::Vector2f
    (Save::square_size * square_coordinates.x,
     Save::square_size * square_coordinates.y));
  positions.push_back (sf::Vector2f
    (Save::square_size * (square_coordinates.x + 1),
     Save::square_size * square_coordinates.y));
  positions.push_back (sf::Vector2f
    (Save::square_size * square_coordinates.x,
     Save::square_size * (square_coordinates.y + 1)));
  positions.push_back (sf::Vector2f
    (Save::square_size * (square_coordinates.x + 1),
     Save::square_size * (square_coordinates.y + 1)));

  for (unsigned i = 0; i < 3; ++i)
    vertex_array[first_position + i] = positions[i];
  for (unsigned i = 0; i < 3; ++i)
    vertex_array[first_position + 3 + i] = positions[i + 1];
}

void
Map::set_vertices_ground_texture_coords (sf::VertexArray &vertex_array,
                                         const unsigned &first_position,
                                         const int &ground_id)
{
  Json::Value texture_dimension = m_grounds_json[to_string (ground_id)];

  if (texture_dimension[2].asUInt () % Save::square_size != 0
      || texture_dimension[3].asUInt () % Save::square_size != 0)
    {
      string error = "The dimensions of ground texture « ";
      error += to_string (ground_id) + " » are not multiples of ";
      error += to_string (Save::square_size) + ".";
      throw runtime_error (error);
    }

  sf::Vector2f top_left (texture_dimension[0].asUInt (), texture_dimension[1].asUInt ());
  sf::Vector2f top_right (texture_dimension[0].asUInt () + Save::square_size,
                          texture_dimension[1].asUInt ());
  sf::Vector2f bottom_left (texture_dimension[0].asUInt (),
                            texture_dimension[1].asUInt () + Save::square_size);
  sf::Vector2f bottom_right (texture_dimension[0].asUInt () + Save::square_size,
                             texture_dimension[1].asUInt () + Save::square_size);

  // cout << "Définition des sommets des textures du sol." << endl;

  // cout << "Haut gauche = " << transformer_vector2f_chaine (top_left) << endl;
  // cout << "Haut droit = " << transformer_vector2f_chaine (top_right) << endl;
  // cout << "Bas gauche = " << transformer_vector2f_chaine (bottom_left) << endl;
  // cout << "Bas droit = " << transformer_vector2f_chaine (bottom_right) << endl;

  vertex_array[first_position].texCoords = top_left;
  vertex_array[first_position + 1].texCoords = top_right;
  vertex_array[first_position + 2].texCoords = bottom_left;
  vertex_array[first_position + 3].texCoords = top_right;
  vertex_array[first_position + 4].texCoords = bottom_left;
  vertex_array[first_position + 5].texCoords = bottom_right;
}

void
Map::set_vertices_surface_texture_coords (std::vector<sf::VertexArray>
                                          &vertex_arrays,
                                          const unsigned &first_position,
                                          const int &item_id,
                                          const unsigned &square_position)
{
  Json::Value dimension_array;
  if (item_id > 0)
    dimension_array = m_items_json[to_string (item_id)];
  else
    {
      for (unsigned i = first_position; i < first_position + 6; ++i)
        vertex_arrays[square_position][i].color = sf::Color (0, 0, 0, 0);
      return;
    }

  if (dimension_array[2].asUInt () % Save::square_size != 0
      || dimension_array[3].asUInt () % Save::square_size != 0)
    {
      string error = "The dimensions of item texture « ";
      error += to_string (item_id) + " » are not multiples of ";
      error += to_string (Save::square_size) + ".";
      throw runtime_error (error);
    }

  unsigned height_in_square = dimension_array[3].asUInt () / Save::square_size;

  sf::Vector2f top_left
    (dimension_array[0].asUInt (),
     dimension_array[1].asUInt () + (height_in_square - 1) * Save::square_size);
  sf::Vector2f top_right (top_left.x + Save::square_size, top_left.y);
  sf::Vector2f bottom_left (top_left.x, top_left.y + Save::square_size);
  sf::Vector2f bottom_right (top_right.x, bottom_left.y);

  // Si la texture déborde sur la case concernée.
  if (dimension_array[3].asUInt () > Save::square_size + (square_position - 1)
      * Save::square_size)
    {
      // Il faut sélectionner le bon morceau.
      sf::Vector2f difference (0, square_position * Save::square_size);
      top_left -= difference;
      top_right -= difference;
      bottom_right -= difference;
      bottom_left -= difference;

      vertex_arrays[square_position][first_position].texCoords = top_left;
      vertex_arrays[square_position][first_position + 1].texCoords = top_right;
      vertex_arrays[square_position][first_position + 2].texCoords = bottom_left;
      vertex_arrays[square_position][first_position + 3].texCoords = top_right;
      vertex_arrays[square_position][first_position + 4].texCoords = bottom_left;
      vertex_arrays[square_position][first_position + 5].texCoords = bottom_right;
    }
  else
    {
      for (unsigned i = first_position; i < first_position + 6; ++i)
        vertex_arrays[square_position][i].color = sf::Color (0, 0, 0, 0);
    }
}

pair<sf::VertexArray, vector<sf::VertexArray>>
Map::get_chunk_vertex_arrays (Chunk &chunk)
{
  shared_ptr<Save::Chunk> save_chunk = chunk.get_save_chunk ();

  sf::VertexArray ground_vertices (sf::Triangles, Save::square_per_side
                                   * Save::square_per_side * 6);
  vector<sf::VertexArray> surface_vertices;

  for (unsigned k = 0; k < m_item_max_height / Save::square_size; ++k)
    {
      surface_vertices.push_back
        (sf::VertexArray (sf::Triangles, Save::square_per_side
                          * Save::square_per_side * 6));
    }

  for (unsigned j = 0; j < Save::square_per_side; ++j)
    {
      for (unsigned i = 0; i < Save::square_per_side; ++i)
        {
          unsigned square_number = i + j * Save::square_per_side;
          int ground_id = save_chunk->get_ground_id ({(int) i, (int) j});

          set_vertices_position (ground_vertices, square_number * 6, {i, j});
          set_vertices_ground_texture_coords
            (ground_vertices, square_number * 6, ground_id);

          for (unsigned k = 0; k < m_item_max_height / Save::square_size; ++k)
            {
              int surface_id = -1;
              if (j + k > Save::square_per_side - 1)
                {
                  if (k > Save::square_per_side)
                    continue;   // Cas non conforme.

                  coordinates chunk_coordinates = chunk.get_coordinates ();
                  chunk_coordinates.y += 1;
                  coordinates square_coordinates = {.x = (int) i,
                                                    .y = (int) j + (int) k - Save::square_per_side};
                  surface_id = m_save.get_surface_item (chunk_coordinates,
                                                        square_coordinates);
                }
              else
                surface_id = save_chunk->get_square_item_id ({i, j + k});

              set_vertices_position (surface_vertices[k],
                                     square_number * 6, {i, j});
              set_vertices_surface_texture_coords
                (surface_vertices, square_number * 6, surface_id, k);
            }
        }
    }

  return make_pair (ground_vertices, surface_vertices);
}

Json::Value
Map::load_json_document (string path)
{
  ifstream document (path);

  if (!document.is_open ())
    throw runtime_error (string ("« ") + path + " » could not be opened.");

  Json::Value root;
  document >> root;
  return root;
}

void
Map::set_item_max_height ()
{
  m_item_max_height = 0;
  for (const string &member : m_items_json.getMemberNames ())
    {
      unsigned current_height = m_items_json[member][3].asUInt ();
      m_item_max_height = (current_height > m_item_max_height) ?
        m_items_json[member][3].asUInt () : m_item_max_height;
    }
}

void
Map::update_chunk_vertices (Chunk &chunk)
{
  pair<sf::VertexArray, vector<sf::VertexArray>> vertices
    = get_chunk_vertex_arrays (chunk);
  chunk.set_vertices (vertices.first, vertices.second);
}

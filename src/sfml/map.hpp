/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016-2018 Corentin Bocquillon <0x539@nybble.fr>
 * Copyright (C) 2016 Loup Fourment
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * Copyright (C) 2018 Cyril Colin <contact@ccolin.fr>
 * Copyright (C) 2018 Baptiste Pouget <baba@firemail.cc>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/
 *
 * @section DESCRIPTION
 *
 * map.hpp is the header of map.cpp.
 */

#pragma once

#include <string>
#include <SFML/Graphics.hpp>
#include <memory>
#include <vector>
#include <utility>
#include <json/json.h>

#include "../structures.hpp"
#include "../save.hpp"

namespace Graphics
{
  class Chunk : public sf::Drawable, public sf::Transformable
  {
  public:
    Chunk (std::shared_ptr<Save::Chunk> me, std::shared_ptr<sf::Texture> items,
           std::shared_ptr<sf::Texture> grounds);

    /**
     * Get the chunk’s coordinates.
     */
    coordinates
    get_coordinates () const;

    std::shared_ptr<Save::Chunk> get_save_chunk ();

    /**
     * Set the chunk’s vertices.
     *
     * @param vertices, the vertices.
     */
    void
    set_vertices (sf::VertexArray ground_vertices,
                  std::vector<sf::VertexArray> surface_vertices);

    /**
     * Set the chunk’s save object.
     *
     * @param chunk, the chunk save object
     */
    void
    set_save_chunk (std::shared_ptr<Save::Chunk> chunk);

    Chunk& operator= (const Chunk &other);

  private:
    sf::VertexArray m_ground_vertices;
    std::vector<sf::VertexArray> m_surface_vertices;
    std::shared_ptr<sf::Texture> m_items;
    std::shared_ptr<sf::Texture> m_grounds;
    std::shared_ptr<Save::Chunk> m_me;

    virtual void
    draw(sf::RenderTarget& target, sf::RenderStates states) const;
  };

  class Map
  {
  public:
    Map (std::string game_name);
    Map (std::string game_name, int seed);
    ~Map ();

    Chunk&
    get_chunk (coordinates coordinates);

    Save::Map&
    get_save ();

    void set_surface_item (coordinates chunk_coordinates,
                           coordinates square_coordinates, int item_id,
                           int quantity = 1);

  private:
    Save::Map m_save;
    std::vector<Chunk> m_chunks;
    Json::Value m_items_json;
    Json::Value m_grounds_json;
    sf::Texture m_items;
    sf::Texture m_grounds;
    unsigned m_item_max_height;

    void
    set_vertices_position (sf::VertexArray &vertex_array,
                           const unsigned &first_position,
                           const coordinates &square_coordinates);

    void
    set_vertices_ground_texture_coords (sf::VertexArray &vertex_array,
                                        const unsigned &first_position,
                                        const int &ground_id);

    /**
     * Set the texture coordinates of the vertices in the given vertex
     * array.
     *
     * @param square_position is the position of the square,
     * e.g. position 0 designate the square, position 1 designate the
     * square under, etc.
     */
    void
    set_vertices_surface_texture_coords (std::vector<sf::VertexArray>
                                         &vertex_arrays,
                                         const unsigned &first_position,
                                         const int &item_id,
                                         const unsigned &square_position);

    /**
     * Get chunk vertex arrays.
     *
     * @return a pair which it’s first element is the ground vertex
     * array and it’s second element is the surface vertex arrays.
     */
    std::pair<sf::VertexArray, std::vector<sf::VertexArray>>
    get_chunk_vertex_arrays (Chunk &chunk);

    Json::Value load_json_document (std::string path);

    void set_item_max_height ();

    void update_chunk_vertices (Chunk &chunk);
  };
}

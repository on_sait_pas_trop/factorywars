/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016-2018 Corentin Bocquillon <0x539@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * menu.cpp display the menus.
 */

#include <exception>
#include <stdexcept>
#include <iostream>

#include "menu.h"

using namespace std;

namespace Menu
{
  TextButton::TextButton (sf::RenderWindow & window, sf::Texture & button_background,
                          sf::Vector2f position, function<void ()> action,
                          string text, int size,
                          sf::Font & font, sf::Color colour)
    : m_window (window), m_background (button_background),
      m_action (action), m_font (font)
  {
    m_text.setString (text);
    m_text.setFont (m_font);
    m_text.setCharacterSize (size);
    m_background_sprite.setTexture (m_background);
    m_background_sprite.setPosition (position);
    create_text (colour);
  }

  bool
  TextButton::contains (int x, int y) const
  {
    return m_background_sprite.getGlobalBounds ().contains (x, y);
  }

  bool
  TextButton::contains (sf::Vector2i coordinates) const
  {
    return m_background_sprite.getGlobalBounds ().contains ((sf::Vector2f) coordinates);
  }

  void
  TextButton::display ()
  {
    m_window.draw (m_background_sprite);
    m_window.draw (m_text);
  }

  void
  TextButton::clicked ()
  {
    m_action ();
  }

  void
  TextButton::create_text (sf::Color colour)
  {
    m_text.setFillColor (colour);
    sf::FloatRect rectangle = m_text.getGlobalBounds ();
    float initial_y = rectangle.top;
    center_rectangle (m_background_sprite.getGlobalBounds (), rectangle);
    rectangle.top -= initial_y;
    m_text.setPosition (rectangle.left, rectangle.top);
  }

  Menu::Menu (sf::RenderWindow & window)
    : m_window (window)
  {

  }

  Menu::~Menu ()
  {

  }



  LoadGameMenu::LoadGameMenu (sf::RenderWindow & window)
    : Menu (window), m_choice (LoadGameMenuChoice::no_choice),
      m_current_highlighted_save (0)
  {
    create_textures ();

    string font_path;
    if (!RUN_IN_PLACE)
      font_path = FONTSDIR"/FreeSans.ttf";
    else
      font_path = "../media/fonts/FreeSans.ttf";

    if (!m_font.loadFromFile (font_path))
      throw runtime_error (font_path + " does not exist");

    populate_saves ();
    if (m_saves.size () == 0)
      m_choice = LoadGameMenuChoice::go_back;
    else
      create_buttons ();
  }

  LoadGameMenu::~LoadGameMenu ()
  {

  }

  void
  LoadGameMenu::display ()
  {
    while (m_choice == LoadGameMenuChoice::no_choice)
      {
        display_menu ();
        handle_events ();
      }
    if (m_saves.size () != 0)
      m_save_path = m_saves[m_current_highlighted_save].first;
  }

  string
  LoadGameMenu::get_save_path ()
  {
    return m_save_path;
  }

  LoadGameMenuChoice
  LoadGameMenu::get_choice ()
  {
    return m_choice;
  }

  void
  LoadGameMenu::create_textures ()
  {
    unsigned width = m_window.getSize ().x;
    unsigned height = m_row_height;
    m_default_button_background_texture.create (width, height);
    m_highlighted_button_background_texture.create (width, height);

    sf::Uint8 pixels[width * height * 4];
    for (unsigned i = 0; i < width * height * 4; i += 4)
      {
        pixels[i] = 0;
        pixels[i+1] = 0;
        pixels[i+2] = 0;
        pixels[i+3] = 255;
      }
    m_default_button_background_texture.update (pixels);

    for (unsigned i = 0; i < width * height * 4; i += 4)
      {
        pixels[i] = 255;
        pixels[i+1] = 0;
        pixels[i+2] = 0;
        pixels[i+3] = 255;
      }
    m_highlighted_button_background_texture.update (pixels);
  }

  void
  LoadGameMenu::create_buttons ()
  {
    m_buttons.clear ();

    sf::Vector2f position (0, 0);

    sf::Texture *texture = &m_default_button_background_texture;
    for (unsigned i = 0; i < m_saves.size (); ++i, position.y += m_row_height)
      {
        if (i == m_current_highlighted_save)
          texture = &m_highlighted_button_background_texture;
        else
          texture = &m_default_button_background_texture;

        m_buttons.emplace_back (m_window,
                                *texture, position,
                                [&, i] ()
                                {
                                  m_current_highlighted_save = i;
                                  m_choice = LoadGameMenuChoice::load_game;
                                },
                                m_saves[i].first + " " + string ("date : ")
                                + m_saves[i].second, 16, m_font,
                                sf::Color::White);
      }
  }

  void
  LoadGameMenu::display_menu ()
  {
    m_window.clear ();
    unsigned maximum_displayable_buttons = m_window.getSize ().y / m_row_height;

    unsigned first_displayed_save;
    if (m_current_highlighted_save > maximum_displayable_buttons)
      first_displayed_save = m_current_highlighted_save;
    else
      first_displayed_save = 0;
    for (unsigned i = first_displayed_save;
         i < maximum_displayable_buttons + m_current_highlighted_save
           && i < m_buttons.size (); ++i)
      m_buttons[i].display ();
    m_window.display ();
  }

  void
  LoadGameMenu::handle_events ()
  {
    sf::Event event;
    bool stay = true;
    while (m_window.pollEvent (event) && stay)
      {
        switch (event.type)
          {
          case sf::Event::Closed:
            m_window.close ();
            m_choice = LoadGameMenuChoice::quit;
            stay = false;
            break;

          case sf::Event::MouseButtonPressed:
            if (event.mouseButton.button != sf::Mouse::Left)
              break;

            for (TextButton &button : m_buttons)
              {
                if (button.contains (event.mouseButton.x, event.mouseButton.y))
                  {
                    button.clicked ();
                    stay = false;
                    break;
                  }
              }
            break;

          case sf::Event::KeyPressed:
            if (event.key.code == sf::Keyboard::Escape)
              {
                m_choice = LoadGameMenuChoice::go_back;
                stay = false;
              }

            else if (event.key.code == sf::Keyboard::Return)
              {
                m_choice = LoadGameMenuChoice::load_game;
                m_save_path = m_saves[m_current_highlighted_save].first;
                stay = false;
              }

            else if (event.key.code == sf::Keyboard::Up)
              {
                if (m_current_highlighted_save > 0)
                  {
                    --m_current_highlighted_save;
                    stay = false;
                    create_buttons ();
                  }
              }
            else if (event.key.code == sf::Keyboard::Down)
              {
                if (m_current_highlighted_save < m_saves.size () - 1)
                  {
                    ++m_current_highlighted_save;
                    stay = false;
                    create_buttons ();
                  }
              }
            break;

          default:
            break;
          }
      }
  }

  void
  LoadGameMenu::populate_saves ()
  {
    string save_name;

    const char *xdg_data_home = getenv ("XDG_DATA_HOME");
    const char *home = getenv ("HOME");

    if (RUN_IN_PLACE)
      save_name = "saves/";

    else if (xdg_data_home == NULL)
      {
        if (home == NULL)
          save_name = "saves/";
        else
          {
            // On utilise la valeur par défaut de XDG_DATA_HOME
            save_name = string (home) + "/.local/share/factorywars/saves/";
          }
      }

    else
      save_name = string (xdg_data_home) + "/factorywars/saves/";

    try
      {
        DirectoryList dir_list (save_name, true);
        m_saves = dir_list.get_files_list ();
      }
    catch (invalid_argument &e)
      {

      }
  }


  InGameMenu::InGameMenu (sf::RenderWindow & window)
    : Menu (window), m_choice (InGameMenuChoice::no_choice)
  {
    load_textures ();
    create_buttons ();
  }

  InGameMenu::~InGameMenu ()
  {

  }

  void
  InGameMenu::display ()
  {
    display_menu ();
    while (m_choice == InGameMenuChoice::no_choice)
      handle_events ();
  }

  InGameMenuChoice
  InGameMenu::get_choice ()
  {
    return m_choice;
  }

  void
  InGameMenu::reset_choice ()
  {
    m_choice = InGameMenuChoice::no_choice;
  }

  void
  InGameMenu::load_textures ()
  {
    string prefix;
    if (RUN_IN_PLACE)
      prefix = "../media/menus";
    else
      prefix = TEXTURESDIR;
    string menu_bg_path = prefix + "/main_menu.png";
    string button_bg_path = prefix + "/button1.png";

    m_background.loadFromFile (menu_bg_path);
    m_background_sprite.setTexture (m_background, true);
    m_button_background.loadFromFile (button_bg_path);

    sf::FloatRect sprite_rect = m_background_sprite.getGlobalBounds ();
    sf::FloatRect window_rect (0, 0, m_window.getSize ().x,
                               m_window.getSize ().y);
    center_rectangle (window_rect, sprite_rect);
    m_background_sprite.setPosition (sprite_rect.left, sprite_rect.top);
  }

  void
  InGameMenu::create_buttons ()
  {
    vector<string> buttons_text = {_("Save"),
                                   _("Save and quit"),
                                   _("Settings"),
                                   _("Quit")};

    sf::FloatRect button_rect (0, 0, m_button_background.getSize ().x,
                               m_button_background.getSize ().y);
    vector<sf::FloatRect> buttons_position (buttons_text.size (), button_rect);
    center_rectangles_vertically (m_background_sprite.getGlobalBounds (),
                                  buttons_position);

    // Actions
    vector<function<void ()>> buttons_action;
    buttons_action.push_back
      ([&] () { m_choice = InGameMenuChoice::save; });
    buttons_action.push_back
      ([&] () { m_choice = InGameMenuChoice::save_quit; });
    buttons_action.push_back (function<void ()> ());
    buttons_action.push_back
      ([&] () { m_choice = InGameMenuChoice::quit; });

    string font_path;
    if (!RUN_IN_PLACE)
      font_path = FONTSDIR"/FreeSans.ttf";
    else
      font_path = "../media/fonts/FreeSans.ttf";

    if (!m_font.loadFromFile (font_path))
      throw runtime_error (font_path + " does not exist");

    int font_size = 40;
    for (unsigned i = 0; i < buttons_text.size (); ++i)
      {
        m_buttons.push_back (TextButton
                             (m_window, m_button_background,
                              sf::Vector2f (buttons_position[i].left,
                                            buttons_position[i].top),
                              buttons_action[i], buttons_text[i],
                              font_size, m_font, sf::Color::White));
      }
  }

  void
  InGameMenu::handle_events ()
  {
    sf::Event event;
    while (m_window.pollEvent (event))
      {
        switch (event.type)
          {
          case sf::Event::Closed:
            m_choice = InGameMenuChoice::quit;
            break;

          case sf::Event::MouseButtonPressed:
            if (event.mouseButton.button != sf::Mouse::Left)
              break;

            for (TextButton &button : m_buttons)
              {
                if (button.contains (event.mouseButton.x, event.mouseButton.y))
                  {
                    button.clicked ();
                    break;
                  }
              }
            break;

          case sf::Event::KeyPressed:
            if (event.key.code == sf::Keyboard::Escape)
              m_choice = InGameMenuChoice::go_back;
            break;

          default:
            break;
          }
      }
  }

  void
  InGameMenu::display_menu ()
  {
    m_window.draw (m_background_sprite);

    for (TextButton &button : m_buttons)
      button.display ();

    m_window.display ();
  }

  MainMenu::MainMenu (sf::RenderWindow & window)
    : Menu (window), m_save_path ("nouvelle sauvegarde"),
      m_load_game_menu (window)
  {
    load_textures ();
    create_buttons ();
    m_choice = MainMenuChoice::no_choice;
  }

  MainMenu::~MainMenu ()
  {

  }

  void
  MainMenu::display ()
  {
    while (m_choice == MainMenuChoice::no_choice)
      {
        display_menu ();
        handle_events ();
      }
  }

  MainMenuChoice
  MainMenu::get_choice ()
  {
    return m_choice;
  }

  string
  MainMenu::get_save_path ()
  {
    return m_save_path;
  }

  void
  MainMenu::load_textures ()
  {
    string prefix;
    if (RUN_IN_PLACE)
      prefix = "../media/menus";
    else
      prefix = TEXTURESDIR;
    string menu_bg_path = prefix + "/main_menu.png";
    string button_bg_path = prefix + "/button1.png";

    m_background.loadFromFile (menu_bg_path);
    m_background_sprite.setTexture (m_background, true);
    m_button_background.loadFromFile (button_bg_path);

    sf::FloatRect sprite_rect = m_background_sprite.getGlobalBounds ();
    sf::FloatRect window_rect (0, 0, m_window.getSize ().x,
                               m_window.getSize ().y);
    center_rectangle (window_rect, sprite_rect);
    m_background_sprite.setPosition (sprite_rect.left, sprite_rect.top);
  }

  void
  MainMenu::create_buttons ()
  {
    vector<string> buttons_text = {_("New game"),
                                   _("Load game"),
                                   _("Settings"),
                                   _("About"),
                                   _("Quit")};

    // Actions
    vector<function<void ()>> buttons_action;
    buttons_action.push_back
      ([&] () { m_choice = MainMenuChoice::new_game; });

    // Doit appeler le menu de chargement de parties.
    // Doit attribuer la valeur MainMenuChoice::load_game à m_choice.
    buttons_action.push_back
      ([&] ()
       {
         m_load_game_menu.display ();
         switch (m_load_game_menu.get_choice ())
           {
           case LoadGameMenuChoice::quit:
             m_choice = MainMenuChoice::quit;
             break;

           case LoadGameMenuChoice::load_game:
             m_choice = MainMenuChoice::load_game;
             m_save_path = m_load_game_menu.get_save_path ();

           default:
             break;
           }
       });

    buttons_action.push_back (function<void ()> ());
    buttons_action.push_back (function<void ()> ());
    buttons_action.push_back
      ([&] () { m_choice = MainMenuChoice::quit; m_window.close ();});

    string font_path;
    if (!RUN_IN_PLACE)
      font_path = FONTSDIR"/FreeSans.ttf";
    else
      font_path = "../media/fonts/FreeSans.ttf";

    if (!m_font.loadFromFile (font_path))
      throw runtime_error (font_path + " does not exist");

    sf::FloatRect button_rect (0, 0, m_button_background.getSize ().x,
                               m_button_background.getSize ().y);
    vector<sf::FloatRect> buttons_position (buttons_text.size (), button_rect);
    center_rectangles_vertically (m_background_sprite.getGlobalBounds (),
                                  buttons_position);

    int font_size = 40;
    for (unsigned i = 0; i < buttons_text.size (); ++i)
      {
        m_buttons.push_back (TextButton
                             (m_window, m_button_background,
                              sf::Vector2f (buttons_position[i].left,
                                            buttons_position[i].top),
                              buttons_action[i], buttons_text[i],
                              font_size, m_font, sf::Color::White));
      }
  }

  void
  MainMenu::handle_events ()
  {
    sf::Event event;
    while (m_window.pollEvent (event))
      {
        switch (event.type)
          {
          case sf::Event::Closed:
            m_window.close ();
            m_choice = MainMenuChoice::quit;
            break;

          case sf::Event::MouseButtonPressed:
            if (event.mouseButton.button != sf::Mouse::Left)
              break;

            for (TextButton &button : m_buttons)
              {
                if (button.contains (event.mouseButton.x, event.mouseButton.y))
                  {
                    button.clicked ();
                    break;
                  }
              }
            break;

          default:
            break;
          }
      }
  }

  void
  MainMenu::display_menu ()
  {
    m_window.clear ();
    m_window.draw (m_background_sprite);

    for (TextButton &button : m_buttons)
      button.display ();

    m_window.display ();
  }
}

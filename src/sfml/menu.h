/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016-2018 Corentin Bocquillon <0x539@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * menu.h is the header of menu.c.
 */

#pragma once

#include <string>
#include <functional>
#include <SFML/Graphics.hpp>

#include "gui_utils.h"
#include "../utils.h"
#include "../gettext.h"
#define _(string) gettext (string)

namespace Menu
{

  /**
   * A button with a centered text inside.
   */
  class TextButton
  {
  public:
    TextButton (sf::RenderWindow & window, sf::Texture & button_background,
                sf::Vector2f position, std::function<void ()> action,
                std::string text, int size,
                sf::Font &font, sf::Color colour);
    bool contains (int x, int y) const;
    bool contains (sf::Vector2i coordinates) const;
    void display ();
    void clicked ();

  private:
    sf::RenderWindow & m_window;
    sf::Texture & m_background;
    std::function<void ()> m_action;
    sf::Text m_text;
    sf::Sprite m_background_sprite;
    sf::Font &m_font;
    int m_size;

    void create_text (sf::Color colour);
  };


  /**
   * An interface for menus.
   */
  class Menu
  {
  public:
    Menu (sf::RenderWindow & window);
    virtual ~Menu ();
    virtual void display () = 0;

  protected:
    sf::RenderWindow & m_window;
  };

  /**
   * Choice for the load game menu.
   */
  enum class LoadGameMenuChoice
  {
    /**
     * No choice.
     */
    no_choice,

    /**
     * The user selected a game.
     */
    load_game,

    /**
     * The user wants to go back.
     */
    go_back,

    /**
     * The user wants to quit.
     */
    quit
  };


  /**
   * The load game menu.
   */
  class LoadGameMenu : public Menu
  {
  public:
    LoadGameMenu (sf::RenderWindow & window);
    virtual ~LoadGameMenu ();
    virtual void display ();
    std::string get_save_path ();
    LoadGameMenuChoice get_choice ();

  private:
    std::string m_save_path;
    LoadGameMenuChoice m_choice;
    sf::Texture m_default_button_background_texture;
    sf::Texture m_highlighted_button_background_texture;
    std::vector<TextButton> m_buttons;
    std::vector<std::pair<std::string,std::string>> m_saves;
    unsigned long m_current_highlighted_save;
    static const unsigned m_row_height = 20;
    sf::Font m_font;

    void create_textures ();
    void create_buttons ();
    void display_menu ();
    void handle_events ();
    void populate_saves ();
  };


  /**
   * Choice for the main menu.
   */
  enum class InGameMenuChoice
  {
    /**
     * No choice.
     */
    no_choice,

    /**
     * The user wants to save the game.
     */
    save,

    /**
     * The user wants to save the game and quit.
     */
    save_quit,

    /**
     * The user wants to save access the settings.
     */
    settings,

    /**
     * The user wants to quit.
     */
    quit,

    /**
     * The user want to return to the game.
     */
    go_back
  };


  class InGameMenu : public Menu
  {
  public:
    InGameMenu (sf::RenderWindow & window);
    virtual ~InGameMenu ();
    virtual void display ();

    InGameMenuChoice get_choice ();
    void reset_choice ();

  private:
    sf::Texture m_background;
    sf::Sprite m_background_sprite;
    sf::Texture m_button_background;
    std::vector<TextButton> m_buttons;
    InGameMenuChoice m_choice;
    sf::Font m_font;

    void load_textures ();
    void create_buttons ();
    void handle_events ();
    void display_menu ();
  };


  /**
   * Choice for the main menu.
   */
  enum class MainMenuChoice
  {
    /**
     * No choice.
     */
    no_choice,

    /**
     * The user wants to start a new game.
     */
    new_game,

    /**
     * The user selected a game to load.
     */
    load_game,

    /**
     * The user wants to quit.
     */
    quit
  };


  class MainMenu : public Menu
  {
  public:
    MainMenu (sf::RenderWindow & window);
    virtual ~MainMenu ();
    virtual void display ();

    MainMenuChoice get_choice ();
    std::string get_save_path ();

  private:
    sf::Texture m_background;
    sf::Sprite m_background_sprite;
    sf::Texture m_button_background;
    std::vector<TextButton> m_buttons;
    MainMenuChoice m_choice;
    std::string m_save_path;
    LoadGameMenu m_load_game_menu;
    sf::Font m_font;

    void load_textures ();
    void create_buttons ();
    void handle_events ();
    void display_menu ();
  };
}

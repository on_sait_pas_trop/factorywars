/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 *
 * @section LICENSE
 *
 * Copyright (C) 2016-2018 Corentin Bocquillon <0x539@nybble.fr>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <map>
#include <string>
#include <functional>
#include <utility>
#include <vector>
#include <SFML/Graphics.hpp>

#include "../structures.hpp"

class Mouse
{
 public:
  Mouse ();
  void handle_mouse_event (sf::Mouse::Button button, bool state, coordinates click_coords);
  void set_event_handler (sf::Mouse::Button button, bool state,
                          std::function<void (coordinates)> handler);

 private:
  std::map<std::pair<sf::Mouse::Button, bool>,
           std::vector<std::function<void (coordinates)>>> m_handlers;

  void trigger_handlers (sf::Mouse::Button button, bool state,
                         coordinates click_coords);
};

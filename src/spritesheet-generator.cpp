/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 *
 * @section LICENSE
 *
 * Copyright (C) 2018 Corentin Bocquillon <corentin@nybble.fr>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * A spritesheet generator.
 */

#include <fstream>
#include <json/json.h>
#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include <stdexcept>

#define MAX_WIDTH 5000

using namespace std;

void
create_spritesheet (string path, vector<string> files_path)
{
  // Il faut lire les textures et sauvegarder leurs dimensions.
  vector<sf::Vector2u> dimensions;
  for (const string &file_path : files_path)
    {
      sf::Image img;
      img.loadFromFile (file_path);
      dimensions.push_back (img.getSize ());
    }

  // Calcul de la taille de la feuille de texture.
  unsigned max_height = 0, total_width = 0;
  for (unsigned i = 0; i < dimensions.size (); ++i)
    {
      sf::Vector2u current_dimensions = dimensions[i];

      if (MAX_WIDTH < current_dimensions.x)
        throw runtime_error (string ("MAX_WIDTH is lower than « ")
                             + files_path[i] + " » width.");

      total_width += current_dimensions.x;

      if (max_height < current_dimensions.y)
        max_height = current_dimensions.y;
    }

  sf::Vector2u spritesheet_size ((total_width < MAX_WIDTH) ? total_width : MAX_WIDTH,
                                 max_height * (1 + total_width / MAX_WIDTH));

  sf::Image spritesheet;
  spritesheet.create (spritesheet_size.x, spritesheet_size.y,
                      sf::Color (0, 0, 0, 0));

  unsigned current_row = 0;
  unsigned current_x = 0;
  Json::Value root;
  for (unsigned i = 0; i < files_path.size (); ++i)
    {
      const string file_path = files_path[i];
      sf::Image img;
      img.loadFromFile (file_path);
      sf::Vector2u size = img.getSize ();
      if (current_x + size.x > MAX_WIDTH)
        {
          current_x = 0;
          ++current_row;
        }
      Json::Value array;
      array[0] = current_x;
      array[1] = current_row * max_height;
      array[2] = size.x;
      array[3] = size.y;
      root[to_string (i + 1)] = array;

      spritesheet.copy (img, current_x, current_row * max_height,
                        sf::IntRect (0, 0, 0, 0), true);
      current_x += size.x;
    }

  spritesheet.saveToFile (path + ".png");

  ofstream output_file (path + ".json");
  output_file << root << endl;
  output_file.close ();
}

int
main ()
{
  ifstream config ("../media/spritesheet-generator.json");
  Json::Value root;
  config >> root;
  config.close ();

  vector<string> members = root.getMemberNames ();
  for (const string &member : members)
    {
      Json::Value member_value = root[member];
      vector<string> tmp_vec;
      for (unsigned i = 0; i < root[member].size (); ++i)
        tmp_vec.push_back (string ("../media/") + member_value[i].asString ());

      create_spritesheet (string ("../media/") + member + "-spritesheet", tmp_vec);
    }

  return 0;
}

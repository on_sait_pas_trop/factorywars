/**
 * @file
 * @author Corentin Bocquillon <0x539@nybble.fr>
 * @author Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 * @author Loup Fourment
 *
 * @section LICENSE
 *
 * Copyright (C) 2016 Corentin Bocquillon <0x539@nybble.fr>
 *
 * Copyright (C) 2016 Loup Fourment
 *
 * Copyright (C) 2016 Pierre Gillet <pierre.gillet+factorywars@linuxw.info>
 *
 * factorywars is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * factorywars is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with factorywars.  If not, see <http://www.gnu.org/licenses/
 *
 * @section DESCRIPTION
 *
 * utils.cpp
 */

#include <errno.h>

#include "utils.h"

using namespace std;

int
insert_line_in_file (char* line, int line_size, int position, const char* file_path, int replace)
{
  int pipe_read_file[2];

  if (pipe (pipe_read_file))
    {
      fprintf (stderr, "Pipe failed.\n");
      return 0;
    }

  write_file_to_pipe (file_path, pipe_read_file[1]);
  const int BUFFER_SIZE = 512;
  char buffer[BUFFER_SIZE];
  memset (buffer, 0, BUFFER_SIZE);

  FILE *file = fopen (file_path, "w");
  if (file == NULL)
    return 0;

  char c;
  int line_number = 0;

  /* On écrit le début du fichier */
  // We write the beginning of the file
  while (line_number != position)
    {
      read (pipe_read_file[0], &c, sizeof (char));
      fwrite (&c, 1, sizeof (char), file);
      if (c == '\n')
	line_number++;
    }
  fwrite (line, line_size, sizeof (char), file);

  /* Si le remplacement est activé, on ignore la ligne suivante */
  // If replace is set to true we ignore the next line
  if (replace)
    {
      do
	{
	  read (pipe_read_file[0], &c, sizeof (char));
	} while (c != '\n');
    }

  int flags = fcntl (pipe_read_file[0], F_GETFL, 0);
  fcntl (pipe_read_file[0], F_SETFL, flags | O_NONBLOCK);

  while (read (pipe_read_file[0], &c, sizeof (char)) > 0)
    {
      fwrite (&c, 1, sizeof (char), file);
    }

  fclose (file);
  return 1;
}

void
write_file_to_pipe (const char* file_path, int pipe)
{
  const int LINE_SIZE = 512;

  FILE *file = fopen (file_path, "r");
  if (file == NULL)
    return;

  char line[LINE_SIZE];

  while (fgets (line, LINE_SIZE, file) != NULL)
    {
      write_to_pipe (pipe, line);
    }

  fclose (file);
}

void
write_to_pipe (int file, const char* message)
{
  write (file, message, strlen (message));
}

int
read_pipe_until_null (char* buffer, size_t buf_size, int pipe)
{
  /* Nombre d’octets */
  /* number of octets */
  int n = 0;

  int flags = fcntl (pipe, F_GETFL, 0);
  fcntl (pipe, F_SETFL, flags | O_NONBLOCK);

  if (read (pipe, buffer, sizeof (char)) == -1)
    {
      fcntl (pipe, F_SETFL, flags);
      return n;
    }
  else
    n++;

  fcntl (pipe, F_SETFL, flags);

  for (unsigned int i = 1; i < buf_size - 1; i++)
    {
      read (pipe, buffer + i, sizeof (char));
      n++;

      if (buffer[i] == '\0')
	break;
    }

  return n;
}

int
get_command_type (const char* data)
{
  int ret = 0;
  char *token = NULL;
  char buffer[strlen (data) + 1];

  strncpy (buffer, data, strlen (data) + 1);

  if (strlen (data) < 1)
    ret = -1;

  /* Il faut extraire la commande. */
  if (ret != -1)
    token = strtok (buffer, " ");
  if (token == NULL)
    ret = -1;
  else if (strcmp (token, "QUIT") == 0)
    ret = 1;
  else if (strcmp (token, "PING") == 0)
    ret = 2;
  else if (strcmp (token, "PONG") == 0)
    ret = 3;
  else if (strcmp (token, "CONNECT") == 0)
    ret = 4;
  else if (strcmp (token, "MOVE") == 0)
    ret = 5;
  else if (strcmp (token, "NEWPLAYER") == 0)
    ret = 6;

  return ret;
}


DirectoryList::DirectoryList (std::string path, bool only_directories)
{
  struct dirent* cur_entry;
  DIR *dir = opendir (path.c_str ());
  if (!dir)
    throw invalid_argument (string (strerror (errno)));

  errno = 0;
  cur_entry = readdir (dir);
  if (!cur_entry)
    {
      closedir (dir);
      throw invalid_argument (string (strerror (errno)));
    }

  bool empty = true;

  /* Pour savoir si c’est un dossier */
  struct stat file_stat;

  /* Pour le temps de dernière modification */
  struct tm *timeinfo;

  while (cur_entry)
    {
      /* On ignore le dossier « . » */
      if (string (cur_entry->d_name) == ".")
	{
	  cur_entry = readdir (dir);
	  continue;
	}

      /* On ignore le dossier « .. » */
      else if (string (cur_entry->d_name) == "..")
	{
	  cur_entry = readdir (dir);
	  continue;
	}
      else
	empty = false;

      if (only_directories)
	{
	  string file_path (path);
	  file_path += string ("/") + cur_entry->d_name;

	  stat (file_path.c_str (), &file_stat);
	  if (!S_ISDIR (file_stat.st_mode))
	    {
	      cur_entry = readdir (dir);
	      continue;
	    }
	}

      char last_modification[128];
      timeinfo = localtime (&file_stat.st_mtime);
      strftime (last_modification, 128, "%FT%X%z", timeinfo);
      m_files.push_back (make_pair (cur_entry->d_name, string (last_modification)));

      cur_entry = readdir (dir);
    }

  if (empty)
    {
      throw invalid_argument ("the save’s directory is empty");
    }
  closedir (dir);
}

DirectoryList::~DirectoryList ()
{

}

std::vector<std::pair<std::string,std::string>>
DirectoryList::get_files_list () const
{
  return m_files;
}


bool
file_exists (string path)
{
  struct stat informations;
  if (stat (path.c_str (), &informations) == -1)
    return false;
  else
    return true;
}

coordinates
get_chunk_coordinates_from_offset (coordinates offset)
{
  const int chunk_width = Save::square_per_side * Save::square_size;

  struct coordinates center_chunk_coordinates;
  center_chunk_coordinates.y = offset.y / chunk_width;
  center_chunk_coordinates.x = offset.x / chunk_width;

  return center_chunk_coordinates;
}
